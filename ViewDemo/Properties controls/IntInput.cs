﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Algorithms;
using Properties;

namespace ViewDemo
{
    public partial class IntInput : UserControl
    {
        private IntProperty info;
        public event EventHandler OnValueChanged;
        public string PropertyName => info.name;
        public IntInput()
        {
            InitializeComponent();
        }
        public IntInput(IntProperty inf, int startValue)
        {

            InitializeComponent();
            info = inf;
            textInput.Text = startValue.ToString();
            barInput.Minimum = info.min;
            barInput.Maximum = info.max;
            barInput.Value = startValue;
            barInput.SmallChange = info.delta;

            groupBox1.Text = info.name;

        }

        public void SetInfo(IntProperty inf)
        {
            info = inf;
            groupBox1.Text = info.name;

            barInput.Minimum = 0;
            barInput.Maximum = info.delta;
        }

        public int GetValue()
        {
            int result;
            if (Int32.TryParse(textInput.Text, out result))
                return result;
           // textInput.Text = 0.ToString();
            return 0;
        }

        private void barInput_ValueChanged(object sender, EventArgs e)
        {
            if (info == null) return;
                textInput.Text = (barInput.Value).ToString(CultureInfo.CurrentCulture);
            OnValueChanged?.Invoke(this, null);

        }

        private void textInput_TextChanged(object sender, EventArgs e)
        {
            OnValueChanged?.Invoke(this, null);
        }
    }
}
