﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Absolut_Model;
using Algorithms;
using Properties;

namespace ViewDemo
{
    public partial class DoubleInput : UserControl
    {
        private DoubleProperty info;
        public event EventHandler OnValueChanged;
        public string PropertyName => info.name;

        public DoubleInput()
        {
            InitializeComponent();
           
        }
        public DoubleInput(DoubleProperty inf, double startValue)
        {
            
            InitializeComponent();
            info = inf;
            textInput.Text = startValue.ToString("N6");
            barInput.Minimum = 0;
            barInput.Maximum = info.delta;
            groupBox1.Text = info.name;
            

        }        

        public void SetInfo(DoubleProperty inf)
        {
            info = inf;
            groupBox1.Text = info.name;
         
            barInput.Minimum = 0;
            barInput.Maximum = info.delta;
        }

        public double GetValue()
        {
            double result;
            if (Double.TryParse(textInput.Text, out result))
                return result;
            textInput.Text = 0.ToString();
            return 0;
        }

        private void barInput_ValueChanged(object sender, EventArgs e)
        {
            if(info == null ) return;
            if (info.deltaType == DoubleDeltaType.Step)
            {
                textInput.Text = (info.min + (info.max - info.min)/info.delta*barInput.Value).ToString("N6",CultureInfo.CurrentCulture);
            }
            if (info.deltaType == DoubleDeltaType.Log)
            {
                textInput.Text = (info.min * Math.Pow((info.max / info.min), (double)barInput.Value / info.delta)  ).ToString("N6", CultureInfo.CurrentCulture);
            }
            OnValueChanged?.Invoke(this, null);
        }

        private void textInput_TextChanged(object sender, EventArgs e)
        {
            OnValueChanged?.Invoke(this, null);
        }
    }
}
