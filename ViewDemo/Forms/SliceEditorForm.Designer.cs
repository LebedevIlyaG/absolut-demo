﻿namespace ViewDemo
{
    partial class SliceEditorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.functionViewControl1 = new ViewDemo.FunctionViewControl();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.firstFreeVarComboBox = new System.Windows.Forms.ComboBox();
            this.secondFreeVarComboBox = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.fixedVars = new System.Windows.Forms.GroupBox();
            this.fixedVarsTable = new System.Windows.Forms.TableLayoutPanel();
            this.createSliceButton = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.fixedVars.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 39.54704F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60.45296F));
            this.tableLayoutPanel1.Controls.Add(this.functionViewControl1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.createSliceButton, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.button2, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 92.65734F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.342658F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(831, 602);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // functionViewControl1
            // 
            this.functionViewControl1.BackColor = System.Drawing.Color.Black;
            this.functionViewControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.functionViewControl1.Location = new System.Drawing.Point(333, 5);
            this.functionViewControl1.Margin = new System.Windows.Forms.Padding(5);
            this.functionViewControl1.Name = "functionViewControl1";
            this.tableLayoutPanel1.SetRowSpan(this.functionViewControl1, 3);
            this.functionViewControl1.Size = new System.Drawing.Size(493, 592);
            this.functionViewControl1.TabIndex = 0;
            this.functionViewControl1.VSync = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.groupBox1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.fixedVars, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 350F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(322, 524);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanel3);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(316, 168);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Free variables";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 152F));
            this.tableLayoutPanel3.Controls.Add(this.firstFreeVarComboBox, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.secondFreeVarComboBox, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.button1, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(310, 149);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // firstFreeVarComboBox
            // 
            this.firstFreeVarComboBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.firstFreeVarComboBox.FormattingEnabled = true;
            this.firstFreeVarComboBox.Location = new System.Drawing.Point(3, 26);
            this.firstFreeVarComboBox.Name = "firstFreeVarComboBox";
            this.firstFreeVarComboBox.Size = new System.Drawing.Size(94, 21);
            this.firstFreeVarComboBox.TabIndex = 0;
            // 
            // secondFreeVarComboBox
            // 
            this.secondFreeVarComboBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.secondFreeVarComboBox.FormattingEnabled = true;
            this.secondFreeVarComboBox.Location = new System.Drawing.Point(161, 26);
            this.secondFreeVarComboBox.Name = "secondFreeVarComboBox";
            this.secondFreeVarComboBox.Size = new System.Drawing.Size(103, 21);
            this.secondFreeVarComboBox.TabIndex = 1;
            // 
            // button1
            // 
            this.tableLayoutPanel3.SetColumnSpan(this.button1, 2);
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Location = new System.Drawing.Point(3, 77);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(304, 69);
            this.button1.TabIndex = 2;
            this.button1.Text = "Confirm";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.FreeVarsButtonClick);
            // 
            // fixedVars
            // 
            this.fixedVars.Controls.Add(this.fixedVarsTable);
            this.fixedVars.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fixedVars.Location = new System.Drawing.Point(3, 177);
            this.fixedVars.Name = "fixedVars";
            this.fixedVars.Size = new System.Drawing.Size(316, 344);
            this.fixedVars.TabIndex = 1;
            this.fixedVars.TabStop = false;
            this.fixedVars.Text = "Slice settings";
            // 
            // fixedVarsTable
            // 
            this.fixedVarsTable.ColumnCount = 1;
            this.fixedVarsTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.fixedVarsTable.Dock = System.Windows.Forms.DockStyle.Top;
            this.fixedVarsTable.Location = new System.Drawing.Point(3, 16);
            this.fixedVarsTable.Name = "fixedVarsTable";
            this.fixedVarsTable.RowCount = 1;
            this.fixedVarsTable.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.fixedVarsTable.Size = new System.Drawing.Size(310, 325);
            this.fixedVarsTable.TabIndex = 0;
            // 
            // createSliceButton
            // 
            this.createSliceButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.createSliceButton.Location = new System.Drawing.Point(3, 533);
            this.createSliceButton.Name = "createSliceButton";
            this.createSliceButton.Size = new System.Drawing.Size(322, 36);
            this.createSliceButton.TabIndex = 2;
            this.createSliceButton.Text = "Confirm";
            this.createSliceButton.UseVisualStyleBackColor = true;
            this.createSliceButton.Click += new System.EventHandler(this.createSliceButton_Click);
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button2.Location = new System.Drawing.Point(3, 575);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(322, 24);
            this.button2.TabIndex = 3;
            this.button2.Text = "Set as main slice";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // SlicesWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(831, 602);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "SliceEditorForm";
            this.Text = "SlicesWindow";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.fixedVars.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private FunctionViewControl functionViewControl1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.ComboBox firstFreeVarComboBox;
        private System.Windows.Forms.ComboBox secondFreeVarComboBox;
        private System.Windows.Forms.GroupBox fixedVars;
        private System.Windows.Forms.Button createSliceButton;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TableLayoutPanel fixedVarsTable;
        private System.Windows.Forms.Button button2;
    }
}