﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Algorithms;
using Functions;
using Properties;
using ViewDemo.Views;

namespace ViewDemo
{
    public partial class StartForm : Form, IExperimentView
    {
        private readonly ExperimentPresenter experimentPresenter;
        private MethodPresenter p;

        public StartForm()
        {
            InitializeComponent();
            experimentPresenter = new ExperimentPresenter();

            experimentPresenter.AddView(this);
        }

        public void ShowNewExperiment(Guid expId, string expName)
        {
            var child = new ExperimentForm(expId) {MdiParent = this, Text = expName};
            // new Thread(() => child.Show()).Start();

            child.Show();
        }

        public void ShowFunctionParams(string functionName, List<PropertyInfo> propertyInfos)
        {
            throw new NotImplementedException();
        }

        public void ShowNewMultiDimExperiment(Guid expId, string expName)
        {
            throw new NotImplementedException();
        }


        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            using (var name = new ExperimentCreationForm())
            {
                var dr = name.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    experimentPresenter.AddNewExperiment(name.experimentName);
                    menuStrip2.Visible = true;
                }
            }
        }

        private void standartMethodToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var c in MdiChildren)
            {
                var form = c as ExperimentForm;
                form?.Stop();
            }
            using (var methodChoose = new MethodChooseDialog(AlgFactory.Options))
            {
                var dr = methodChoose.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    var child = ActiveMdiChild as ExperimentForm;
                    if (child != null)
                    {
                        var methodPresenter = new MethodPresenter(child.ExpId);
                        methodPresenter.SetMethodWithOldFunction(methodChoose.ChoosedOption);
                    }
                }
            }
        }


        private void standartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var c in MdiChildren)
            {
                var form = c as ExperimentForm;
                form?.Stop();
            }
            using (var methodChoose = new MethodChooseDialog(FunctionFactory.Options))
            {
                var dr = methodChoose.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    var child = ActiveMdiChild as ExperimentForm;
                    if (child != null)
                    {
                        var methodPresenter = child.Presenter;
                        methodPresenter.SetMainFunction(methodChoose.ChoosedOption);
                    }
                }
            }
        }

        private void parametersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var child = ActiveMdiChild as ExperimentForm;
            foreach (var c in MdiChildren)
            {
                var form = c as ExperimentForm;
                form?.Stop();
            }
            if (child != null)
            {
                var methodPresenter = child.Presenter;
                var values = new Dictionary<PropertyInfo, object>();
                foreach (var i in methodPresenter.GetParamsInfo())
                    values.Add(i, methodPresenter.GetProperty(i.name));
                using (var paramsChoose = new ParametersDialog(values))
                {
                    var dr = paramsChoose.ShowDialog();
                    if (dr == DialogResult.OK)
                        methodPresenter.SetProperties(paramsChoose.changedValues);
                }
            }
        }

        private void newProblemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var c in MdiChildren)
            {
                var form = c as ExperimentForm;
                form?.Stop();
            }
            using (var methodChoose = new FormulaInputDialog())
            {
                var dr = methodChoose.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    var child = ActiveMdiChild as ExperimentForm;
                    if (child != null)
                    {
                        var methodPresenter = child.Presenter;
                        methodPresenter.SetMainFunction(methodChoose.LeftBoundary, methodChoose.RightBoundary,
                            methodChoose.Formula);
                    }
                }
            }
        }

        private void dLLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var c in MdiChildren)
            {
                var form = c as ExperimentForm;
                form?.Stop();
            }
            using (var methodChoose = new DLLSettings())
            {
                var dr = methodChoose.ShowDialog();
                if (dr == DialogResult.OK)
                    using (var xml = new OpenFileDialog() {Title = "Open .dll file"})
                    {
                        
                            var child = ActiveMdiChild as ExperimentForm;
                            if (child != null)
                            {
                                var methodPresenter = child.Presenter;
                                try
                                {
                                    methodPresenter.SetMainFunction(methodChoose.dllPath, methodChoose.configPath);
                                }
                                catch (Exception exception)
                                {
                                    Console.Write(exception);
                                    MessageBox.Show("Cant open dll");
                                }
                            }
                       
                    }
            }
        }

        private void examinToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var c in MdiChildren)
            {
                var form = c as ExperimentForm;
                form?.Stop();
            }
            using (var examin = new ExaminSettings())
            {
                var dr = examin.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    var child = ActiveMdiChild as ExperimentForm;
                    if (child != null)
                    {
                        var methodPresenter = child.Presenter;
                        methodPresenter.SetExaminMethod(examin.dllPath, examin.configPath, examin.examinPath);
                    }
                }
            }
        }

        private void cascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void mosaicToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void horizontalMosaicToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void directToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var c in MdiChildren)
            {
                var form = c as ExperimentForm;
                form?.Stop();
            }
            using (var examin = new ExaminSettings())
            {
                var dr = examin.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    var child = ActiveMdiChild as ExperimentForm;
                    if (child != null)
                    {
                        var methodPresenter = child.Presenter;
                        methodPresenter.SetDirectMethod(examin.dllPath, examin.configPath, examin.examinPath);
                    }
                }
            }
        }
    }
}