﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Functions;
using Functions.Functions;

namespace ViewDemo
{
    public partial class SliceEditorForm : Form
    {
        private readonly Dictionary<string, int> variablesNames = new Dictionary<string, int>();
        private int _freeVar1;
        private int _freeVar2;
        private IFunction _functionToShow;
        private IFunction _functionToSlice;

        private bool isFreeVariablesInputedCorrectly;
        public Dictionary<int, IFunction> SlicePlanes;

        public SliceEditorForm()
        {
            InitializeComponent();
            FunctionToSlice = new Squares();
        }

        public SliceEditorForm(IFunction functionToSlice)
        {
            InitializeComponent();
            FunctionToSlice = functionToSlice;
        }

        public IFunction FunctionToSlice
        {
            get { return _functionToSlice; }
            set
            {
                _functionToSlice = value;
                UpdateFreeVarsControls();
            }
        }

        private void UpdateSlice()
        {
            throw new NotImplementedException();
        }

        private void UpdateFreeVarsControls()
        {
            variablesNames.Clear();
            for (var i = 0; i < _functionToSlice.Left.Count; i++)
                variablesNames.Add("x" + i, i);

            firstFreeVarComboBox.DataSource = variablesNames.Keys.ToList();
            secondFreeVarComboBox.DataSource = variablesNames.Keys.ToList();
        }

        private void FreeVarsButtonClick(object sender, EventArgs e)
        {
            var selectedFreeVar1 = variablesNames[firstFreeVarComboBox.Text];
            var selectedFreeVar2 = variablesNames[secondFreeVarComboBox.Text];
            if (selectedFreeVar2 != selectedFreeVar1)
            {
                if (selectedFreeVar1 < selectedFreeVar2)
                {
                    _freeVar1 = selectedFreeVar1;
                    _freeVar2 = selectedFreeVar2;
                }
                else
                {
                    _freeVar1 = selectedFreeVar2;
                    _freeVar2 = selectedFreeVar1;
                }
                isFreeVariablesInputedCorrectly = true;
                var button = sender as Button;
                button.BackColor = Color.Green;
                UpdateFixedVarsControls();
            }
            else
            {
                isFreeVariablesInputedCorrectly = false;
                var button = sender as Button;
                button.BackColor = Color.Red;
                fixedVarsTable.Controls.Clear();
                SlicePlanes = null;
            }
        }

        private void UpdateFixedVarsControls()
        {
            for (var i = 0; i < _functionToSlice.Left.Count; i++)
            {
                if (i == _freeVar1 || i == _freeVar2)
                    continue;
                var control = new SliceEditorControl("x" + i, "x" + _freeVar1, "x" + _freeVar2) {Dock = DockStyle.Fill};
                fixedVarsTable.Controls.Add(control);
            }
            Refresh();
        }

        private void createSliceButton_Click(object sender, EventArgs e)
        {
            SlicePlanes = new Dictionary<int, IFunction>();
            foreach (var control in fixedVarsTable.Controls)
            {
                var t = control as SliceEditorControl;
                var planeParams = t.GetPlaneParameters();
                if (planeParams == null)
                {
                    var button = sender as Button;
                    button.BackColor = Color.Red;
                    SlicePlanes = null;
                    return;
                }
                IFunction plane = new Plane(planeParams.Item1, planeParams.Item2, planeParams.Item3,
                    _functionToSlice.Left, _functionToSlice.Right);
                SlicePlanes.Add(t.GetVarIndex(), plane);
            }
            var button1 = sender as Button;
            button1.BackColor = Color.Green;

            _functionToShow = new FixedByFormulaFunction(_functionToSlice, SlicePlanes);
            functionViewControl1.SetFunction(_functionToShow);
            Refresh();
        }
    }
}