﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ViewDemo
{
    public partial class FormulaInputDialog : Form
    {
        public List<double> LeftBoundary;
        public List<double> RightBoundary;
        public string Formula;

        public FormulaInputDialog()
        {
            InitializeComponent();
        }

        private void OK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            LeftBoundary = new List<double>() {Convert.ToDouble(leftX.Text), Convert.ToDouble(leftY.Text) };
            RightBoundary = new List<double>() { Convert.ToDouble(rightX.Text), Convert.ToDouble(rightY.Text) };
            Formula = textBox1.Text;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
