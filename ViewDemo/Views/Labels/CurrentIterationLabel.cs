﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Functions;
using Properties;

namespace ViewDemo.Labels
{
    public partial class CurrentIterationLabel : UserControl, IMethodView
    {
        public CurrentIterationLabel()
        {
            InitializeComponent();
        }

        public void UpdateMethodProperties()
        {
            
        }

        public void UpdateIterarion(int curIter)
        {
            curIterLabel.Text = curIter.ToString();
        }

        public void CreateCustomFunctionMenu(string functionName, Dictionary<PropertyInfo, object> info)
        {
            
        }

        public void СreateUsualFunctionMenu()
        {
         
        }

        public void ShowAdditionalFunction(IFunction f)
        {
            
        }
    }
}
