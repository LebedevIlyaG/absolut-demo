﻿using System;
using System.Collections.Generic;
using Algorithms;
using Graphics;
using OpenTK;

namespace ViewDemo
{
    public class PointsOnPlot2D : PointsSet2D, IExperimentPointsView
    {
        private double[] _center;
        private bool _isBoundsSet;
        private double[] _max;
        private double[] _min;
        private int lastBin = -1;


        public void SetBounds(int dims, double[] min, double[] max, double[] center)
        {
            _min = min;
            _max = max;
            _center = center;
            _isBoundsSet = true;
        }

        public void ClearPoints()
        {
            foreach (var b in bins)
                b.Value.Clear();
            bins.Clear();
        }

        public void AddPoint(MethodPoint p)
        {
            if (!_isBoundsSet) throw new ArgumentOutOfRangeException();
            lastBin = p.id_process;
            if (!bins.ContainsKey(p.id_process)) bins.Add(p.id_process, new List<Vector3d>());
            bins[p.id_process].Add(new Vector3d(2 * (p.x[0] - _center[0]) / (_max[0] - _min[0]), 0,
                2 * (p.x[1] - _center[1]) / (_max[1] - _min[1])));
        }

        public void DeleteLastPoint(MethodPoint p)
        {
            if (lastBin != -1)
                bins[lastBin].RemoveAt(bins[lastBin].Count - 1);
        }
    }
}