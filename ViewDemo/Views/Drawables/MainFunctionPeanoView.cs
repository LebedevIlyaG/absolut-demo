﻿using Functions;
using Graphics;

namespace ViewDemo
{
    public class MainFunctionPeanoView : Curve, IMainFunctionView
    {
        public MainFunctionPeanoView() : base(new SimplePainter())
        {
        }

        public void SetBounds(int dims, double[] min, double[] max, double[] center)
        {
        }

        public void SetGrid(int dims, int gridSize, double[] x, double[] y, double[,] z)
        {
        }

        public void SetFunction(IFunction f)
        {
            SetCurve(f);
        }
    }
}