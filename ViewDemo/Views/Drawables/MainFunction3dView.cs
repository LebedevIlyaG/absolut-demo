﻿using Functions;
using Graphics;

namespace ViewDemo
{
    public class MainFunction3dView : Surface, IMainFunctionView
    {
        public MainFunction3dView() : base(new SimplePainter())
        {
        }

        public void SetBounds(int dims, double[] min, double[] max, double[] center)
        {
        }

        public void SetGrid(int dims, int gridSize, double[] x, double[] y, double[,] z)
        {
            //double[] newX = new double[gridSize], newY = new double[gridSize];
            //double[,] newZ = new double[gridSize,gridSize];
            //Array.Copy(x, newX, gridSize);
            //Array.Copy(y, newY, gridSize);
            //Array.Copy(z, newZ,gridSize*gridSize);
            //Utils.AdjustTo01(newX,newY,newZ,gridSize);
            //SetCurve(gridSize,newX,newY,newZ);
        }

        public void SetFunction(IFunction f)
        {
            SetSurface(f);
        }
    }
}