﻿using System;
using Functions;
using Graphics;

namespace ViewDemo
{
    public class MainFunctionTemperatureMapView : TemperatureMap, IMainFunctionView
    {
        public MainFunctionTemperatureMapView() : base(new SimplePainter())
        {
        }

        public void SetBounds(int dims, double[] min, double[] max, double[] center)
        {
        }

        public void SetGrid(int dims, int gridSize, double[] x, double[] y, double[,] z)
        {
            double[] newX = new double[gridSize], newY = new double[gridSize];
            var newZ = new double[gridSize, gridSize];
            Array.Copy(x, newX, gridSize);
            Array.Copy(y, newY, gridSize);
            Array.Copy(z, newZ, gridSize * gridSize);
            Utils.AdjustTo01(newX, newY, newZ, gridSize);
            SetSurface(gridSize, newX, newY, newZ);
        }

        public void SetFunction(IFunction f)
        {
        }
    }
}