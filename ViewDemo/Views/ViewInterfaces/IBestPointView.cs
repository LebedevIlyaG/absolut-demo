﻿using Algorithms;

namespace ViewDemo
{
    public interface IBestPointView
    {
        void UpdateBestPoint(MethodPoint bestPoint);
        void SetBounds(int dims, double[] min, double[] max, double[] center);
    }
}