﻿using Functions;
using Graphics;

namespace ViewDemo
{
    public class FunctionViewControl : DrawablesControl
    {
        private  FunctionView points = new FunctionView();

        public FunctionViewControl(IFunction f)
        {
            points = new FunctionView(f);
            AddDrawableObject(points);
        }
        public FunctionViewControl()
        {
            AddDrawableObject(points);
        }

        public void SetFunction(IFunction f)
        {
            points.SetSurface(f);
          //  AddDrawableObject(points);
        }

    }
}