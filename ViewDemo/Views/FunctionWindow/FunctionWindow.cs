﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Functions;

namespace ViewDemo
{
    public partial class FunctionWindow : Form
    {
        public FunctionWindow()
        {
            InitializeComponent();
        }
        public FunctionWindow(IFunction f)
        {
            InitializeComponent();
            function2dView1.SetFunction(f);
            functionViewControl1.SetFunction(f);
           

        }
    }
}
