﻿using System;
using Functions;
using Graphics;


namespace ViewDemo
{
    public class FunctionView : Surface
    {
        private int _gridSize = 64;
        public FunctionView(IPainter p) : base(p)
        {
           
        }
        public FunctionView(IFunction f) : base(new SimplePainter())
        {
            SetSurface(f);
        }
        public FunctionView() : base(new SimplePainter())
        {
            
        }
        public FunctionView(int gridPointsCount, double[] x, double[] y, double[,] z) : base(gridPointsCount, x, y, z)
        {
        }

        public FunctionView(int gridPointsCount, double[] x, double[] y, double[,] z, IPainter p) : base(gridPointsCount, x, y, z, p)
        {
        }

        
    }
}