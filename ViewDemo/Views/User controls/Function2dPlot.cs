﻿using System;
using Graphics;
using OpenTK;

namespace ViewDemo.Views
{
    internal class Function2dPlot : DrawablesControlThreading
    {
        private readonly MainFunctionPeanoView _function3DView = new MainFunctionPeanoView();
        private readonly PointsOnPlotPeano _points = new PointsOnPlotPeano();
        private readonly CoordinatesDistributionViewPeano _x = new CoordinatesDistributionViewPeano();
        private readonly  CoordinatesDistributionViewPeano _y = new CoordinatesDistributionViewPeano();
        private ExperimentPointsPresenter _experimentPointsPresenter;
        private Guid _expId;
        private readonly int _gridSize = 64;
        private MainFunctionPresenter _mainFunctionPresenter;

        public Function2dPlot()
        {
            Camera =  new Camera(new Vector3d(0, 0, 6.5));
            AddDrawableObject(_points);
            AddDrawableObject(_function3DView);
            AddDrawableObject(_x);
            AddDrawableObject(_y);
            _x.SetAxis(Vector3d.UnitX);
            _y.SetAxis(Vector3d.UnitY);
        }

        public Guid ExpId
        {
            get { return _expId; }
            set
            {
                if(value == Guid.Empty) return;
                if (_experimentPointsPresenter != null || _mainFunctionPresenter != null)
                    throw new InvalidOperationException();
                _expId = value;
                _experimentPointsPresenter = new ExperimentPointsPresenter(_expId, _gridSize);
                _mainFunctionPresenter = new MainFunctionPresenter(_expId, _gridSize);
               _experimentPointsPresenter.AddView(_points);
                _experimentPointsPresenter.AddView(_x);
                _experimentPointsPresenter.AddView(_y);
                _mainFunctionPresenter.AddView(_function3DView);

            }
        }
    }
}