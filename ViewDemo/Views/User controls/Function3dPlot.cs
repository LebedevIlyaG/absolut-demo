﻿using System;
using Graphics;

namespace ViewDemo.Views
{
    internal class Function3dPlot : DrawablesControlThreading
    {
        private readonly MainFunction3dView _function3DView = new MainFunction3dView();
        private readonly PointsOnPlot _points = new PointsOnPlot();
        private ExperimentPointsPresenter _experimentPointsPresenter;
        private Guid _expId;
        private readonly int _gridSize = 64;
        private MainFunctionPresenter _mainFunctionPresenter;

        public Function3dPlot()
        {
            AddDrawableObject(_points);
            AddDrawableObject(_function3DView);
        }

        public Guid ExpId
        {
            get { return _expId; }
            set
            {
                if (value == Guid.Empty) return;
                if (_experimentPointsPresenter != null || _mainFunctionPresenter != null)
                    throw new InvalidOperationException();
                _expId = value;
                _experimentPointsPresenter = new ExperimentPointsPresenter(_expId, _gridSize);
                _mainFunctionPresenter = new MainFunctionPresenter(_expId, _gridSize);
                _experimentPointsPresenter.AddView(_points);
                _mainFunctionPresenter.AddView(_function3DView);
            }
        }
    }
}