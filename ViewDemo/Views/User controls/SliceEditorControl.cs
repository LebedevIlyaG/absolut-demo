﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Functions;

namespace ViewDemo
{
    public partial class SliceEditorControl : UserControl
    {
        public SliceEditorControl()
        {
            InitializeComponent();
        }

        public SliceEditorControl(String var, string free1, string free2)
        {
            InitializeComponent();
            varLabel.Text = var;
            var1.Text = free1;
            var2.Text = free2;
        }

        public int GetVarIndex()
        {
            return Convert.ToInt32(varLabel.Text.Substring(1));
        }
        public Tuple<double,double,double> GetPlaneParameters()
        {
            try
            {
                double coef1 = Convert.ToDouble(var1CoefTextBox.Text);
                double coef2 = Convert.ToDouble(var2CoefTextBox.Text);
                double constValue = Convert.ToDouble(constTextBox.Text);
                return new Tuple<double, double, double>(coef1,coef2,constValue);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }
    }
}
