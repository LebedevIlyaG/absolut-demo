// This is the main DLL file.

#include "stdafx.h"
#include <msclr\marshal_cppstd.h>
#include "ProblemManager.h"

#include "ExaminProblem.h"

ProblemManager::ProblemManager::ProblemManager()
{
    manager = new TProblemManager();
}

int ProblemManager::ProblemManager::LoadProblemLibrary(String ^ libPath)
{
    return manager->LoadProblemLibrary(msclr::interop::marshal_as<std::string>(libPath));
}

ProblemInterface ^ ProblemManager::ProblemManager::GetProblem()
{
    ExaminProblem ^ f;
    f = gcnew ExaminProblem(manager->GetProblem());
    return f;
}

ProblemManager::ProblemManager::~ProblemManager()
{
    delete manager;
}
