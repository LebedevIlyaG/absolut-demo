using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Functions;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using Properties;

namespace Graphics
{
    public class Curve : IDrawable, IPropertyUser
    {
        private readonly IPainter _p;
        private bool _isGridSet;
        protected PropertyProvider _Property = new PropertyProvider();
        private List<Vector3> _points = new List<Vector3>();
       
        private IFunction f;

        public Curve(IPainter p)
        {
            _p = p;
            _Property.RegisterProperty(new BoolProperty("Adaptive grid", true));
            _Property.RegisterProperty(new BoolProperty("Axis", true));
            _Property.RegisterProperty(new ColorProperty("Mesh color", Color.Black));
            _Property.RegisterProperty(new IntProperty("Grid points", 8, 256, 8, 128));
           
        }

        public void Draw()
        {
            if (_isGridSet && _p != null) DrawSurface();
        }

        public string Name => "Curve";


        public void SetProperty(string name, object value)
        {
            _Property.SetProperty(name, value);
        }


        public void SetProperties(Dictionary<string, object> propsValues)
        {
            _Property.SetProperties(propsValues);
            if (propsValues.ContainsKey("Grid points") )
                FillPoints(f);
        }

        public object GetProperty(string name) => _Property.GetProperty(name);
        public List<PropertyInfo> GetPropertiesInfo() => _Property.GetPropertiesInfo();

        public void RegisterProperty(PropertyInfo p) => _Property.RegisterProperty(p);

        public void SetCurve(IFunction func)
        {
            FillPoints(func);
            _isGridSet = true;
        }

        private void FillPoints(IFunction func)
        {
            _points.Clear();
            f = func;
            int gridPoints = (int) GetProperty("Grid points");

            float step = 1f/ (gridPoints - 1);
            for (var i = 0; i < gridPoints; i++)
            {
               float x = step * i;
               var y = (float) func.Calc(x);
                _points.Add(new Vector3(x,y, 0));
            }
            float ymin = _points.Min(p => p.Y);
            float ymax = _points.Max(p => p.Y);
            float xCenter = 0.5f;
            float yCenter = (ymin + ymax) / 2;
            for (int i = 0; i < gridPoints; i++)
            {
                _points[i] = new Vector3((_points[i].X - xCenter) * 2, (_points[i].Y - yCenter)*2 / (ymax - ymin),0);
            }
           
        }

        private void DrawSurface()
        {
            GL.PushMatrix();
            //GL.LoadIdentity();
           // GL.Ortho(0.0f, 500, 200, 0.0f, 0.0f, 1.0f);

           // GL.Scale(2, 2, -1);
           // GL.Translate(-1, -1, 0);
            if ((bool) GetProperty("Axis")) DrawAxis();
            GL.LineWidth(1);
            GL.Begin(BeginMode.LineStrip);
            foreach (var point in _points)
            {
                GL.Color3(Color.Black);
                GL.Vertex3(point);
            }
            GL.End();
            GL.PopMatrix();
        }
        

        protected void DrawAxis()
        {
            float shift = 0.05f;
            _p.DrawTriangle(new Vector3(-1.2f - shift, 1.2f - shift, 0), Color.Black,
                new Vector3(-1.2f, 1.2f, 0), Color.Black,
                new Vector3(-1.2f + shift, 1.2f - shift, 0), Color.Black);
            _p.DrawTriangle(new Vector3(1.2f - shift, -1.2f + shift, 0), Color.Black,
                new Vector3(1.2f, -1.2f, 0), Color.Black,
                new Vector3(1.2f - shift, -1.2f - shift, 0), Color.Black);
            GL.LineWidth(1);
            GL.Begin(BeginMode.Lines);
            GL.Color3(Color.Black);
            GL.Vertex3(-1.2, -1.2, 0);
            GL.Color3(Color.Black);
            GL.Vertex3(1.2, -1.2, 0);
            GL.Color3(Color.Black);
            GL.Vertex3(-1.2, -1.2, 0);
            GL.Color3(Color.Black);
            GL.Vertex3(-1.2, 1.2, 0);
            GL.End();
        }
    }
}