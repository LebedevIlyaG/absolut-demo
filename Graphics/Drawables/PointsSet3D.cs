﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Reflection;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Graphics
{
    public static class MyExtensions
    {
        public static Vector3d Vector3d(this List<double> list, double y = 0, double z = 0)
        {
            switch (list.Count)
            {
                case 0:
                    throw new ArgumentException("list is empty");
                case 1:
                    return new Vector3d(list[0], y, z);
                case 2:
                    return new Vector3d(list[0], list[1], z);
                default:
                    return new Vector3d(list[0], list[1], list[2]);
            }
        }

/*
        public static Vector3d[,] ToGrid(this IFunction f, int numberOfPoints)
        {
            //double deltax = f.Right[0] - f.
            //for (int i = 0; i < numberOfPoints; i++)
            //{
            //    for (int j = 0; j < numberOfPoints; j++)
            //    {

            //    }
            //}
        }
*/
    }

    public class PointsSet3D : IDrawable
    {
        private readonly IPainter painter;
        //protected readonly List<Vector3d> points;
        protected readonly Dictionary<int, List<Vector3d>> bins;
        private List<Color> allColors = new List<Color>() {Color.Red, Color.Blue, Color.Indigo, Color.DeepPink, Color.DodgerBlue, Color.LightSeaGreen, Color.Cyan, Color.Magenta};
        public PointsSet3D(List<List<double>> data, IPainter pointsPainter)
        {
            painter = pointsPainter;
           // points = new List<Vector3d>();
            bins = new Dictionary<int, List<Vector3d>>();
            
        }
        public PointsSet3D()
        {
            painter = new SimplePainter();
           // points = new List<Vector3d>();
            bins = new Dictionary<int, List<Vector3d>>();
           
        }

       

        public void Draw()
        {
            GL.PushMatrix();
            GL.Scale(1, 1, -1);

            for (int i = 0; i < bins.Count; i++)
            {
                foreach (var t in bins[i])
                {
                    painter.DrawColorPoint(t, allColors[i]);
                }
            }
            
            GL.PopMatrix();
        }

        public string Name => "Points";
      
    }
}