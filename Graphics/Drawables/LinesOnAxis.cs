﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using Properties;

namespace Graphics
{
    public class LinesOnAxis : IDrawable, IPropertyUser
    {
        protected PropertyProvider PropertyProvider = new PropertyProvider();
        private readonly IPainter painter;
        protected readonly List<Vector3d> points;
        private Vector3d _axis = Vector3d.UnitX;
        protected double start = 1;
        protected double shifth = 0;
        protected double lineSize = 0.2;
        protected Color lineColor = Color.Navy;
        
        public LinesOnAxis()
        {
            painter = new SimplePainter();
            points = new List<Vector3d>();
            RegisterProperty(new ColorProperty("Line color", Color.Navy));
        }
        
        public void SetAxis(Vector3d axis)
        {
            _axis = axis;
        }

       
        public void Draw()
        {
            
           
            Vector3d start, end;
            if (_axis.Z != 0)
            {
                GL.PushMatrix();
                GL.Scale(1,1,-1);
            }
            foreach (var t in points)
            {
               GL.LineWidth(0.1f);
                var color = (Color)GetProperty("Line color");
                if (_axis.X != 0)
                {
                    start = new Vector3d(t.X, 0, this.start + shifth);
                    end = new Vector3d(t.X, 0, 1+shifth+lineSize);
                    painter.DrawLine(start,end,color);
                }
                else if (_axis.Y != 0)
                {
                    start = new Vector3d(-1 - shifth, t.Y, 0);
                    end = new Vector3d( - 1 - shifth-lineSize, t.Y, 0);
                    painter.DrawLine(start, end, color);
                }
                else if (_axis.Z != 0)
                {
                    start = new Vector3d(-1 - shifth, 0, t.Z);
                    end = new Vector3d( - 1 - shifth - lineSize, 0, t.Z);
                    painter.DrawLine(start, end, color);
                }
            }
            if (_axis.Z != 0)
            {
                GL.PopMatrix();
            }
        }

        public string Name => "Lines on axis";
        public List<DrawablePropertyInfo> PropertyInfos { get; }
        public void SetProperty(string name, object value)
        {
           PropertyProvider.SetProperty(name, value);
        }

        public void SetProperties(Dictionary<string, object> propsValues)
        {
            PropertyProvider.SetProperties(propsValues);
        }

        public object GetProperty(string name)
        {
            return PropertyProvider.GetProperty(name);
        }

        public List<PropertyInfo> GetPropertiesInfo()
        {
            return PropertyProvider.GetPropertiesInfo();
        }

        public void RegisterProperty(PropertyInfo p)
        {
            PropertyProvider.RegisterProperty(p);
        }
    }
}
