﻿using System.Collections.Generic;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Graphics
{
    public class PointsSet2D : IDrawable
    {
        private readonly IPainter painter;
        protected readonly Dictionary<int, List<Vector3d>> bins;
        private List<Color> allColors = new List<Color>() { Color.Red, Color.Blue, Color.Indigo, Color.DeepPink, Color.DodgerBlue, Color.LightSeaGreen, Color.Cyan, Color.Magenta };

        public PointsSet2D(List<List<double>> data, IPainter pointsPainter)
        {
            painter = pointsPainter;
            bins = new Dictionary<int, List<Vector3d>>();
        }
        public PointsSet2D()
        {
            painter = new SimplePainter();
            bins = new Dictionary<int, List<Vector3d>>();
        }

        public void Draw()
        {
            GL.PushMatrix();
            GL.Scale(1, 1, -1);
            GL.Begin(BeginMode.Points);
            for (int i = 0; i < bins.Count; i++)
            {
                foreach (var t in bins[i])
                {
                    GL.Color3(allColors[i]);
                    GL.Vertex3(t);
                }
            }
            GL.End();
            GL.PopMatrix();
        }

        public string Name => "Points2d";
      
    }
}