﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphics
{
    public enum DrawablesPropertiesTypes
    {
        Boolean
    };

    public struct DrawablePropertyInfo
    {
        public string Name;
        public DrawablesPropertiesTypes Type;

        public DrawablePropertyInfo(string name, DrawablesPropertiesTypes type)
        {
            Name = name;
            Type = type;
        }

    }

}
