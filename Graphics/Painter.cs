using System;
using System.Collections.Generic;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Graphics
{
    public class SimplePainter : IPainter
    {
        public void DrawPoint(Vector3d a)
        {
            GL.Begin(BeginMode.Points);
            GL.Color3(Color.Red);
            GL.Vertex3(a.X, a.Y, a.Z);
            GL.End();
        }

        public void DrawColorPoint(Vector3d a, Color c)
        {
            GL.Begin(BeginMode.Points);
            GL.Color3(c);
            GL.Vertex3(a);
            GL.End();
        }

        public void DrawTriangled(Vector3d a, Color aColor, Vector3d b, Color bColor, Vector3d c, Color cColor,
            bool normals = false)
        {
            GL.Begin(BeginMode.Triangles);


            GL.Color3(aColor);
            if (normals)
            {
                var normal = Vector3d.Cross(b - a, c - a);
                GL.Normal3(normal.Normalized());
            }
            GL.Vertex3(a);
            GL.Color3(bColor);
            if (normals)
            {
                var normal = Vector3d.Cross(b - a, c - a);
                GL.Normal3(normal.Normalized());
            }

            GL.Vertex3(b);
            GL.Color3(cColor);
            if (normals)
            {
                var normal = Vector3d.Cross(b - a, c - a);
                GL.Normal3(normal.Normalized());
            }

            GL.Vertex3(c);
            GL.End();
        }

        public void DrawTriangle(Vector3 a, Color aColor, Vector3 b, Color bColor, Vector3 c, Color cColor,
            bool normals = false)
        {
            GL.Begin(BeginMode.Triangles);


            GL.Color3(aColor);
            if (normals)
            {
                var normal = Vector3.Cross(b - a, c - a);
                GL.Normal3(normal.Normalized());
            }
            GL.Vertex3(a);
            GL.Color3(bColor);
            if (normals)
            {
                var normal = Vector3.Cross(b - a, c - a);
                GL.Normal3(normal.Normalized());
            }

            GL.Vertex3(b);
            GL.Color3(cColor);
            if (normals)
            {
                var normal = Vector3.Cross(b - a, c - a);
                GL.Normal3(normal.Normalized());
            }

            GL.Vertex3(c);
            GL.End();
        }
        public void DrawMaterialTriangle(Vector3d a, Color aColor, Vector3d b, Color bColor, Vector3d c, Color cColor,
            bool normals = false)
        {
            GL.Begin(BeginMode.Triangles);

            GL.ColorMaterial(MaterialFace.Front, ColorMaterialParameter.Diffuse);
            GL.Color3(aColor);
            if (normals)
            {
                var normal = Vector3d.Cross(b - a, c - a);
                GL.Normal3(normal.Normalized());
            }
            GL.Vertex3(a);
            GL.Color3(bColor);
            if (normals)
            {
                var normal = Vector3d.Cross(b - a, c - a);
                GL.Normal3(normal.Normalized());
            }

            GL.Vertex3(b);
            GL.Color3(cColor);
            if (normals)
            {
                var normal = Vector3d.Cross(b - a, c - a);
                GL.Normal3(normal.Normalized());
            }

            GL.Vertex3(c);
            GL.End();
        }

        public void DrawRectangle(Vector3d a, Vector3d b, Vector3d c, Vector3d d)
        {
            throw new NotImplementedException();
        }

        public void InvertZAxis()
        {
            GL.Scale(1, 1, -1);
        }

        public void DrawCylinder(Vector3d start, Vector3d end, double radius, double height, Color color)
        {
            var segments = 10; // Higher numbers improve quality 
            // The radius (width) of the cylinder

            var initialDirection = new Vector3d(0, 1, 0);
            var translatedDirection = initialDirection + start;
            var rotateAngle = Vector3d.CalculateAngle(translatedDirection, end - start);
            var rotationAxis = Vector3d.Cross(translatedDirection, end - start);
            rotationAxis = rotationAxis.Normalized();
            var vertices = new List<Vector3>();
            for (double y = 0; y < 2; y++)
                for (double x = 0; x < segments; x++)
                {
                    var theta = x/(segments - 1)*2*Math.PI;

                    vertices.Add(new Vector3
                    {
                        X = (float) (radius*Math.Cos(theta)),
                        Y = (float) (height*y),
                        Z = (float) (radius*Math.Sin(theta))
                    });
                }

            var indices = new List<int>();
            for (var x = 0; x < segments - 1; x++)
            {
                indices.Add(x);
                indices.Add(x + segments);
                indices.Add(x + segments + 1);

                indices.Add(x + segments + 1);
                indices.Add(x + 1);
                indices.Add(x);
            }
            GL.PushMatrix();
            GL.Translate(start);
            GL.Rotate(MathHelper.RadiansToDegrees(rotateAngle), rotationAxis);
            GL.Begin(BeginMode.Triangles);
            GL.Color3(color);
            foreach (var index in indices)
                GL.Vertex3(vertices[index]);
            GL.End();
            var d1 = height/20;
            Vector3d pyramidEnd = new Vector3d(start.X, start.Y+height+d1, start.Z);
            var d2 = height/50;
            var a = new Vector3d(start.X - d2, start.Y+height, start.Z - d2);
            var b = new Vector3d(start.X - d2, start.Y+height, start.Z + d2);
            var c = new Vector3d(start.X + d2, start.Y+height, start.Z + d2);
            var d = new Vector3d(start.X + d2, start.Y+height, start.Z - d2);
            GL.Begin(BeginMode.Triangles);
            GL.Vertex3(a);
            GL.Vertex3(b);
            GL.Vertex3(c);
            GL.Vertex3(a);
            GL.Vertex3(c);
            GL.Vertex3(d);

            GL.Vertex3(a);
            GL.Vertex3(pyramidEnd);
            GL.Vertex3(b);

            GL.Vertex3(b);
            GL.Vertex3(pyramidEnd);
            GL.Vertex3(c);

            GL.Vertex3(d);
            GL.Vertex3(pyramidEnd);
            GL.Vertex3(c);

            GL.Vertex3(a);
            GL.Vertex3(pyramidEnd);
            GL.Vertex3(d);
            GL.End();
            GL.PopMatrix();
        }

        public void DrawLineLoop(Vector3d a, Vector3d b, Vector3d c, Vector3d d, Color color, float lineWidth = 1)
        {
            GL.LineWidth(lineWidth);
            GL.Begin(BeginMode.LineLoop);
            GL.Color3(color);
            GL.Vertex3(a);
            GL.Vertex3(b);
            GL.Vertex3(c);
            GL.Vertex3(d);
            GL.End();
        }

        public void DrawLine(Vector3d a, Vector3d b, Color lineColor, float lineWidth)
        {
            GL.LineWidth(lineWidth);
            GL.Begin(BeginMode.Lines);
            GL.Color3(lineColor);
            GL.Vertex3(a);
            GL.Color3(lineColor);
            GL.Vertex3(b);
            GL.End();
        }

        public void DrawTriangle(Vector3d a, Vector3d b, Vector3d c)
        {
            throw new NotImplementedException();
        }

        private void DrawBoxes(float xmin, float ymin, float ymax, float xmax)
        {
            var zmin = 0;
            float zmax = 100;
            var down = 0.25f;
            // DrawBottomPoints(zmin, algPoints);
            // DrawAlgPoints(Vector3ds);
            var lineColor = Color.Black;
            float lineWidth = 1;
            var a = new Vector3d(xmin, ymin, zmin);
            var ua = new Vector3d(xmin, ymin, zmax);
            var b = new Vector3d(xmin, ymax, zmin);
            var ub = new Vector3d(xmin, ymax, zmax);
            var c = new Vector3d(xmax, ymax, zmin);
            var uc = new Vector3d(xmax, ymax, zmax);
            var d = new Vector3d(xmax, ymin, zmin);
            var ud = new Vector3d(xmax, ymin, zmax);
            DrawLineLoop(a, b, c, d, lineColor, lineWidth);
            DrawLineLoop(ua, ub, uc, ud, lineColor, lineWidth);
            DrawLine(a, ua, lineColor, lineWidth);
            DrawLine(b, ub, lineColor, lineWidth);
            DrawLine(c, uc, lineColor, lineWidth);
            DrawLine(d, ud, lineColor, lineWidth);
        }
    }
}