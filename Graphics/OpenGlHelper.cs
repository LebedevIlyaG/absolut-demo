﻿using System;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Graphics
{
    public static class OpenGlHelper
    {
        public static void Init3dWithSimpleLight()
        {
            GL.ShadeModel(ShadingModel.Smooth);
            GL.ClearColor(SystemColors.Control);
            GL.ClearDepth(1.0);
            GL.Enable(EnableCap.DepthTest);

            GL.DepthFunc(DepthFunction.Lequal);
            GL.Enable(EnableCap.Normalize);
            GL.Enable(EnableCap.ColorMaterial);

            GL.LightModel(LightModelParameter.LightModelTwoSide, 1);
            GL.Hint(HintTarget.PerspectiveCorrectionHint, HintMode.Nicest);
            float[] LightAmbient = {0.6f, 0.6f, 0.6f};
            float[] LightPosition = {0, 1.0f, 0.0f, 0};
            float[] LightDiffuse = {0.5f, 0.5f, 0.5f};
            GL.Enable(EnableCap.Light0);
            GL.Light(LightName.Light0, LightParameter.Ambient, LightAmbient);
            GL.Light(LightName.Light0, LightParameter.Diffuse, LightDiffuse);
            GL.Light(LightName.Light0, LightParameter.Position, LightPosition);

            GL.LineWidth(3);
            GL.PointSize(3);

            GL.Enable(EnableCap.PointSmooth);
            GL.Hint(HintTarget.PointSmoothHint, HintMode.Nicest);
            // Сглаживание линий
            GL.Enable(EnableCap.LineSmooth);
            GL.Hint(HintTarget.LineSmoothHint, HintMode.Nicest);
            // Сглаживание полигонов    
            GL.Enable(EnableCap.PolygonSmooth);
            GL.Hint(HintTarget.PolygonSmoothHint, HintMode.Nicest);
            //GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            //GL.Enable(EnableCap.Blend);
        }

        public static void ResizeGLControl(GLControl c)
        {
            if (c.Height == 0)
                c.Height = 1;
            if (c.Width == 0)
                c.Width = 1;
            var min = Math.Min(c.Height, c.Width);
            var x = 0;
            var y = 0;
            if (c.Height > c.Width)
                y = (c.Height - c.Width) / 2;

            else
                x = (c.Width - c.Height) / 2;
            GL.Viewport(x, y, min, min);
            //GL.MatrixMode(MatrixMode.Projection);
            ////GL.LoadIdentity();
            var prespectiveMatrix = Matrix4d.CreatePerspectiveFieldOfView(45 * Math.PI / 180, 1, 0.1, 100);
            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadMatrix(ref prespectiveMatrix);
            //GL.LoadIdentity();
        }
    }
}