﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Functions
{
    public class MultidimFunction : IFunction
    {
        public List<double> Right { get; set; }
        public List<double> Left { get; set; }
        public List<double> Min_x { get; set; }
        private PeanoEvolvent p;
        public virtual double Calc(List<double> arg)
        {
            throw new System.NotImplementedException();
        }

        public double Calc(double x)
        {
            return Calc(GetImage(x));
        }

        public List<double> GetImage(double x)
        {
            if (p == null)
            {
                p = new PeanoEvolvent(Left.Count, 19);
                p.SetBounds(Left.ToArray(), Right.ToArray());
            }
            double[] image;
            p.GetImage(x, out image);
            if(!this.CheckPoint(image.ToList())) throw new ArgumentOutOfRangeException();
            return image.ToList();
        }

        public double GetPrototype(List<double> x)
        {
            double[] image = x.ToArray();
            double pr_x = 0;
            if (p == null)
            {
                p = new PeanoEvolvent(Left.Count, 19);
                p.SetBounds(Left.ToArray(), Right.ToArray());
            }
            p.GetPreimages(image, out pr_x);
            return pr_x;
        }
    }
}