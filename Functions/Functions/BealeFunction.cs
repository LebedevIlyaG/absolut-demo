﻿using System;
using System.Collections.Generic;

namespace Functions.Functions
{
    internal class BealeFunction : MultidimFunction
    {
        public BealeFunction()
        {
            Right = new List<double> {4.5, 4.5};
            Left = new List<double> {-4.5, -4.5};
        }

        public override double Calc(List<double> arg)
        {
          //  if (!this.CheckPoint(arg)) throw new ArgumentException();
            return (1.5 - arg[0] + arg[0] * arg[1]) * (1.5 - arg[0] + arg[0] * arg[1]) +
                   (2.25 - arg[0] + arg[0] * arg[1] * arg[1]) * (2.25 - arg[0] + arg[0] * arg[1] * arg[1]) +
                   (2.65 - arg[0] + arg[0] * arg[1] * arg[1] * arg[1]) *
                   (2.65 - arg[0] + arg[0] * arg[1] * arg[1] * arg[1]);
        }
    }
}