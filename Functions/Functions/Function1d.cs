﻿using System;
using System.Collections.Generic;

namespace Functions
{
    class Function1d : IFunction
    {
        public List<double> Right { get; set; }
        public List<double> Left { get; set; }
        public List<double> Min_x { get; set; }
        public virtual double Calc(List<double> arg)
        {
            if(!this.CheckPoint(arg))throw new ArgumentOutOfRangeException();
            return Calc(arg[0]);
        }

        public virtual double Calc(double x)
        {
            return 0;
        }

        public  List<double> GetImage(double x)
        {
            if (!this.CheckPoint(new List<double>() { x })) throw new ArgumentOutOfRangeException();
            return new List<double>() {x};
        }
        public double GetPrototype(List<double> x)
        {
            if (!this.CheckPoint(x)) throw new ArgumentOutOfRangeException();
            return x[0];
        }

    }
}