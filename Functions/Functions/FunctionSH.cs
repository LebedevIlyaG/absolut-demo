﻿using System;
using System.Collections.Generic;

namespace Functions.Functions
{
    internal class FunctionSh : Function1d
    {
        private readonly double[] _a = new double[10];
        private readonly double[] _c = new double[10];
        private readonly double[] _k = new double[10];

        public FunctionSh()
        {
            Left = new List<double>();
            Right = new List<double>();
            Left.Add(0.0);
            Right.Add(10.0);
            var rand = new Random();

            for (var i = 0; i < 10; i++)
            {
                _k[i] = rand.NextDouble() * (3 - 1) + 1;
                _a[i] = rand.NextDouble() * 10;
                _c[i] = rand.NextDouble() * 10;
            }
        }

        public override double Calc(double x)
        {
            if (!this.CheckPoint(new List<double> {x})) throw new ArgumentOutOfRangeException();
            double sum = 0;
            for (var i = 0; i < 10; i++)
                sum += 1 / (_k[i] * (x - _a[i]) * (x - _a[i]) + _c[i]);
            return -1 * sum;
        }
    }
}