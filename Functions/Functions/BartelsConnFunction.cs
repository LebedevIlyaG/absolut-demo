﻿using System;
using System.Collections.Generic;

namespace Functions.Functions
{
    internal class BartelsConnFunction : MultidimFunction
    {
        public BartelsConnFunction()
        {
            Right = new List<double> {500, 500};
            Left = new List<double> {-500, -500};
        }

        public override double Calc(List<double> arg)
        {
            //  if (!this.CheckPoint(arg)) throw new ArgumentException();
            return Math.Abs(arg[0] * arg[0] + arg[0] * arg[1] + arg[1] * arg[1]) +
                   Math.Abs(Math.Cos(arg[1]) + Math.Abs(Math.Sin(arg[0])));
        }
    }
}