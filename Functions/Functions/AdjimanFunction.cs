﻿using System;
using System.Collections.Generic;

namespace Functions.Functions
{
    internal class AdjimanFunction : MultidimFunction
    {
        public AdjimanFunction()
        {
            Right = new List<double> {2, 1};
            Left = new List<double> {-1, -1};
        }

        public override double Calc(List<double> arg)
        {
            //if (!this.CheckPoint(arg)) throw new ArgumentException();
            return Math.Cos(arg[0]) * Math.Sin(arg[1]) - arg[0] / (arg[1] * arg[1] + 1);
        }
    }
}