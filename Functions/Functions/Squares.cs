﻿using System;
using System.Collections.Generic;

namespace Functions.Functions
{
    public class Squares : MultidimFunction
    {
        public Squares()
        {
            Left = new List<double>() {1,1,1,1};
            Right = new List<double>() {10,10,10,10};
        }

        public override double Calc(List<double> arg)
        {
            return arg[0]*arg[0] + arg[1]*arg[1] + arg[2]*arg[2]*arg[3] +arg[3]*arg[3];
        }
    }
}