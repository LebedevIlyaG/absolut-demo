﻿namespace Absolut_Model
{
    public interface IEvent
    {
        void OnPointsReset();
        void OnMainFunctionChanged();
        void OnSlicesChanged();
        void OnMethodChanged();
        void OnNextStep();
        void OnPrevStep();
        void OnExperimentAdded();
        void OnExperimentDeleted();
    }
}