﻿namespace Absolut_Model
{
    public static class ModelFactory
    {
        private static readonly IModel m = new Model();

        public static IModel Build()
        {
            return m;
        }
    }
}