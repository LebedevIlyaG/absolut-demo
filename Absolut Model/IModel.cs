﻿using System;
using System.Collections.Generic;
using Algorithms;
using Functions;
using Properties;

namespace Absolut_Model
{
    public interface IModel
    {
        void Subscribe(IEvent e);
        void Unsubcribe(IEvent e);
        Dictionary<Guid, string> GetExperimentsNames();
        MethodPoint GetLastPoint(Guid experimentId);
        Guid AddExperiment(string name, ISearchAlg alg);
        void DeleteExperiment(Guid experimentId);
        void SubscribeExperiment(IEvent e, Guid experimentId);
        IEnumerable<MethodPoint> GetExpEnumerator(Guid experimentId, int start = 0); //TODO исправить итератор
        ISearchAlg GetAlg(Guid experimentId); //TODO what with const? // const ISearchAlg getAlgStuction(Guid)?
        void SetAlg(Guid experimentId, ISearchAlg alg);
        void SetProperty(string name, object value, Guid experimentId);
        void NextStep(Guid experimentId);
        void PrevStep(Guid experimentId);
        object GetProperty(string name, Guid experimentId);
        List<PropertyInfo> GetPropsNames(Guid experimentId);
        MethodPoint GetBestPoint(Guid experimentId);
        int GetCurrentInter(Guid experimentId);
        void SetProperties(Guid experimentId, Dictionary<string, object> propsValues);
        bool HasNext(Guid experimentId);
        void SetSlicesDictionary(Dictionary<int, IFunction> slices, Guid expId);
        Dictionary<int, IFunction> GetSlicesDictionary(Guid expId);


    }
}