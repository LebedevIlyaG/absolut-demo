﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Properties
{
    public class PropertyProvider : IPropertyUser
    {
        protected Dictionary<string, object> properties = new Dictionary<string, object>();
        protected readonly List<PropertyInfo> PropertyInfos = new List<PropertyInfo>();

        public string Name => "Property Provider";

        public void SetProperty(string name, object value)
        {
            if (!properties.ContainsKey(name)) throw new ArgumentException("Can not set non existing property");
            PropertyInfo info = PropertyInfos.Find(t => t.name == name);
            if (!info.CheckValue(value)) throw new ArgumentException("Can not set wrong value");
            properties[name] = value;
        }

        public void SetProperties(Dictionary<string, object> propsValues)
        {
            foreach (var p in propsValues)
            {
                SetProperty(p.Key, p.Value);
            }
        }

        public object GetProperty(string name)
        {
            if (!properties.ContainsKey(name)) throw new ArgumentException("Property does not exist");
            return properties[name];
        }

        public List<PropertyInfo> GetPropertiesInfo()
        {
            return PropertyInfos;
        }

        public void RegisterProperty(PropertyInfo p)
        {
            if (PropertyInfos.Contains(p)) throw new ArgumentException("Can not register an existing property");
            PropertyInfos.Add(p);
            properties.Add(p.name, p.defaultValue);
        }
    }
}
