﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Functions;

namespace Algorithms.Methods
{
    class CharacteristicAlgParal : GenericAlg
    {
        protected IFunction f;
        protected int maxCharIndex;
        protected List<Methods.Characteristic> Characteristic_list;

        public CharacteristicAlgParal()
        {

            Characteristic_list = new List<Methods.Characteristic>();
            RegisterProperty(new DoubleProperty("Precision", 0.000001, 0.1, 5, DoubleDeltaType.Log, 0.00001));
            RegisterProperty(new FuncProperty("Function", null));
            RegisterProperty(new IntProperty("Process", 1, 4, 3, 1));

        }

        public override void SetProperty(string name, object value)
        {
            if (name == "Function")
            {
                f = (IFunction)value;
            }
            base.SetProperty(name, value);

        }

        protected virtual double CalcCharacteristic(int curIntervalIndex)
        {
            return 0;
        }

        protected virtual double CalcNewPoint()
        {
            return 0;
        }
        public override void RecalcPoints()
        {
            if (f == null) return;
            Restart();
            ExperimentPoints.Clear();
            Init();

            while (realIteration <= (int)AlgProperties["Maximum Iteration"])
            {
                ExperimentPoints.Sort((p1, p2) => p1.evolventX.CompareTo(p2.evolventX));
                maxCharIndex = 0;
                
                Characteristic_list.Clear();
                for (int i = 0; i < realIteration; i++)
                {
                    var R = CalcCharacteristic(i);

                }
                Characteristic_list.Sort((p1, p2) => p2.value_characteristic.CompareTo(p1.value_characteristic));

                int index = 0;
                if ((int)AlgProperties["Process"] > Characteristic_list.Count) index = Characteristic_list.Count;
                else index = (int)AlgProperties["Process"];


                for (int i = 0; i < index; i++)
                {


                    double newPoint = CalcNewPoint();


                    if (IsEnded()) return;
                    maxCharIndex = i + 1;

                    ExperimentPointsReal.Add(new MethodPoint()
                    {
                        evolventX = newPoint,
                        IdFun = 0,
                        iteration = realIteration + 1,
                        y = f.Calc(newPoint),
                        x = f.GetImage(newPoint),
                        id_process = i
                    });

                    ExperimentPoints.Add(new MethodPoint()
                    {
                        evolventX = newPoint,
                        IdFun = 0,
                        iteration = realIteration + 1,
                        y = f.Calc(newPoint),
                        x = f.GetImage(newPoint),
                        id_process = i
                    });

                }
                realIteration++;
            }
        }

        public override bool IsEnded()
        {
            // if (Math.Abs(ExperimentPoints[maxCharIndex].evolventX - ExperimentPoints[maxCharIndex - 1].evolventX)
            //     < (double)AlgProperties["Precision"])
            if (Math.Abs(Characteristic_list[maxCharIndex].right_x.evolventX - Characteristic_list[maxCharIndex].left_x.evolventX)
                    < (double)AlgProperties["Precision"])
                return true;
            return false;
        }

        private void Init()
        {
            ExperimentPoints.Add(new MethodPoint()
            {
                IdFun = 1,
                iteration = 0,
                isEnd = AlgEnd.no,
                x = f.Left,
                y = f.Calc(f.Left),
                evolventX = 0
            });
            ExperimentPoints.Add(new MethodPoint()
            {
                IdFun = 1,
                iteration = 1,
                isEnd = AlgEnd.no,
                x = f.Right,
                y = f.Calc(f.Right),
                evolventX = 1
            });
            ExperimentPointsReal.Add(new MethodPoint()
            {
                IdFun = 1,
                iteration = 0,
                isEnd = AlgEnd.no,
                x = f.Left,
                y = f.Calc(f.Left),
                evolventX = 0
            });
            ExperimentPointsReal.Add(new MethodPoint()
            {
                IdFun = 1,
                iteration = 1,
                isEnd = AlgEnd.no,
                x = f.Right,
                y = f.Calc(f.Right),
                evolventX = 1
            });
            realIteration = 1;
        }
    }
}
