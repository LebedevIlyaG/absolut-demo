﻿using System;
using System.Collections.Generic;
using Functions;

namespace Algorithms
{
    public class Kushnir_method_1 : BaseAlg
    {
        private readonly List<double> argmin;

        private readonly List<double> characteristic;
        private double m;
        private double min;
        private readonly List<double> next_x;
        private double x1;
        private double x2;

        public Kushnir_method_1(int _maxiter = 200, double _precision = 0.00001, double _parametr = 2,
            int _multidimensionality = 1)
        {
            MaxIterations = _maxiter;
            Precision = _precision;
            Parameter = _parametr;
            multidimensionality = _multidimensionality;
           // Func = new FunctionHL();
            argmin = new List<double>();
            next_x = new List<double>();
            characteristic = new List<double>();
            AllPoints = new List<MethodPoint>();
            AllPoints_number = new List<MethodPoint>();
            PropertiesList = new Dictionary<string, PropertyList_class>();
            PropertiesList_d = new Dictionary<string, double>();
            PropertiesList_i = new Dictionary<string, int>();
            PropertiesList_f = new Dictionary<string, IFunction>();
            InitPropertyList();
            RecalcPoints();
        }

        public string Name
        {
            get { return "Kushnir_method"; }
        }

        public IFunction Func { get; set; }
        public double Parameter { get; set; }
        public double Precision { get; set; }
        public List<IFunction> Restrictions { get; set; }

        public override void InitPropertyList() //protected?
        {
            PropertiesList.Add("Parametr_d", new PropertyList_class(PropertyType.Double, 0.001, 2, 0.001));
            PropertiesList.Add("Precision", new PropertyList_class(PropertyType.Double, 0, 1, Precision));
            PropertiesList.Add("Function", new PropertyList_class(PropertyType.Func, Func.Left[0], Func.Right[0], 0.0));
            PropertiesList.Add("MaxIter", new PropertyList_class(PropertyType.Integer, 0, MaxIterations + 1000, 1));
            if (Parameter <= 0) Parameter = 1.1;
            if (Precision <= 0) Precision = 0.000001;
            if (Precision > 1) Precision = 0.000001;
            if (MaxIterations < 0) MaxIterations = 0;
            PropertiesList_d.Add("Parametr_d", Parameter);
            PropertiesList_d.Add("Precision", Precision);
            PropertiesList_f.Add("Function", Func);
            PropertiesList_i.Add("MaxIter", MaxIterations);
        }

        private MethodPoint CreatePoint(List<double> x, int IDFunc, string s)
        {
            var p1 = new MethodPoint();
            p1.x.Add(x[0]);
            p1.IdFun = IDFunc;
            p1.y = Func.Calc(x);
            if (s == "no") p1.isEnd = AlgEnd.no;
            else if (s == "iter") p1.isEnd = AlgEnd.iter;
            else if (s == "presicion") p1.isEnd = AlgEnd.presicion;
            return p1;
        }

        public void Init()
        {
            Func = GetFuncProperty("Function");
            AllPoints.Add(CreatePoint(Func.Left, 0, "no"));
            AllPoints.Add(CreatePoint(Func.Right, 0, "no"));
            AllPoints_number.Add(CreatePoint(Func.Left, 0, "no"));
            AllPoints_number.Add(CreatePoint(Func.Right, 0, "no"));
            characteristic.Add(0);
            characteristic.Add(0);
            if (Func.Calc(Func.Left) < Func.Calc(Func.Right))
            {
                min = Func.Calc(Func.Left);
                argmin.Add(Func.Left[0]);
            }
            else
            {
                min = Func.Calc(Func.Right);
                argmin.Add(Func.Right[0]);
            }

            x1 = Func.Left[0];
            x2 = Func.Right[0];
            next_x.Insert(0, 0);
            m = -100000;
        }

        public void Restart_in()
        {
            Iterations = -1;
            AllPoints.Clear();
            AllPoints_number.Clear();
            characteristic.Clear();
            argmin.Clear();
            next_x.Clear();
            min = Func.Calc(Func.Left);
            x1 = Func.Left[0];
            x2 = Func.Right[0];
            m = -100000;
        }

        public override bool IsEnded()
        {
            if (Math.Abs(x1 - x2) < Precision)
                return true;
            return false;
        }

        public override void RecalcPoints()
        {
            Restart_in();
            //InitPropertyList();
            Init();
            Parameter = GetDoubleProperty("Parametr_d");
            Precision = GetDoubleProperty("Precision");

            var iter = 0;
            while (!IsEnded() && (iter != MaxIterations))
            {
                double m = 1;
                var num_i = 1;
                var number = 1;

                var Maxr = -4.0*(min - Parameter - AllPoints[1].y)*(min - Parameter - AllPoints[0].y)/
                           (AllPoints[1].x[0] - AllPoints[0].x[0]);
                var Maxr1 = Maxr;
                var i = 0;
                while (i < AllPoints.Count - 1)
                {
                    Maxr = -4.0*(min - Parameter - AllPoints[i + 1].y)*(min - Parameter - AllPoints[i].y)/
                           (AllPoints[i + 1].x[0] - AllPoints[i].x[0]);

                    if (Maxr > Maxr1)
                    {
                        Maxr1 = Maxr;
                        number = num_i;
                    }
                    i++;
                    num_i++;
                }

                x1 = AllPoints[number - 1].x[0];
                x2 = AllPoints[number].x[0];

                next_x[0] = x1 +
                            (x2 - x1)*(min - Parameter - AllPoints[number].y)/
                            (2.0*(min - Parameter) - AllPoints[number].y - AllPoints[number - 1].y);

                var index = 0;
                for (var j = 0; j < AllPoints.Count; j++)
                {
                    if (AllPoints[j].x[0] < next_x[0]) index++;
                }

                var p = new MethodPoint();
                // p.x = next_x;
                p.x.Add(next_x[0]);
                p.y = Func.Calc(next_x);
                if (iter == MaxIterations) p.isEnd = AlgEnd.iter;
                else if (IsEnded()) p.isEnd = AlgEnd.presicion;
                else
                {
                    p.isEnd = AlgEnd.no;
                }

                AllPoints.Insert(index, p);
                AllPoints_number.Add(p);
                //characteristic.Insert(index, 0);
                if (min > Func.Calc(next_x))
                {
                    min = Func.Calc(next_x);
                    argmin[0] = next_x[0];
                }
                iter++;
            }
            real_iteration = iter;
        }

        ~Kushnir_method_1()
        {
            argmin.Clear();
            next_x.Clear();
            characteristic.Clear();
            AllPoints.Clear();
            PropertiesList.Clear();
            PropertiesList_d.Clear();
            PropertiesList_i.Clear();
            PropertiesList_f.Clear();
            AllPoints_number.Clear();
        }

        public override void SetDoubleProperty(string name, double value)
        {
            if (PropertiesList.ContainsKey(name))
            {
                if (name == "Parametr_d")
                {
                    Parameter = value;
                    if (PropertiesList_d.ContainsKey(name))
                    {
                        PropertiesList_d[name] = value;
                    }
                    else PropertiesList_d.Add("Parametr_d", value);
                }
                if (name == "Precision")
                {
                    Precision = value;
                    if (PropertiesList_d.ContainsKey(name))
                    {
                        PropertiesList_d[name] = value;
                    }
                    else PropertiesList_d.Add("Precision", value);
                }
            }
        }

        public override void SetIntegerProperty(string name, int value)
        {
            if (PropertiesList.ContainsKey(name))
            {
                if (name == "MaxIter")
                {
                    MaxIterations = value;
                    if (PropertiesList_i.ContainsKey(name))
                    {
                        PropertiesList_i[name] = value;
                    }
                    else PropertiesList_i.Add("MaxIter", value);
                }
            }
        }

        public override void SetFuncProperty(string name, IFunction value)
        {
            if (PropertiesList.ContainsKey(name))
            {
                if (name == "Function")
                {
                    Func = value;
                    if (PropertiesList_f.ContainsKey(name))
                    {
                        PropertiesList_f[name] = value;
                    }
                    else PropertiesList_f.Add("Function", value);
                }
            }
        }

        public override double GetDoubleProperty(string name)
        {
            if (PropertiesList.ContainsKey(name))
            {
                if (name == "Parametr_d") return PropertiesList_d[name];
                if (name == "Precision") return PropertiesList_d[name];
            }
            return 0;
        }

        public override int GetIntegerProperty(string name)
        {
            if (PropertiesList.ContainsKey(name))
            {
                if (name == "MaxIter") return PropertiesList_i[name];
            }
            return 0;
        }

        public override IFunction GetFuncProperty(string name)
        {
            if (PropertiesList.ContainsKey(name))
            {
                if (name == "Function") return PropertiesList_f[name];
            }
            return null;
        }

        public override Dictionary<string, PropertyList_class> GetPropertiesNames()
        {
            return PropertiesList;
        }
    }
}