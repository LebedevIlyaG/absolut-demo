﻿using System;
using System.Collections.Generic;
using System.Linq;
using Functions;
using Properties;

namespace Algorithms
{
    class AgpPeano : GenericAlg
    {

        private IFunction f;
        private int maxCharIndex = 1;

        public AgpPeano()
        {
            RegisterProperty(new DoubleProperty("Precision", 0.000001, 0.1, 5, DoubleDeltaType.Log, 0.00001));
            RegisterProperty(new FuncProperty("Function", null));
            RegisterProperty(new DoubleProperty("r", 1.000001, 100, 99, DoubleDeltaType.Step, 2));
           

        }

        public override void SetProperty(string name, object value)
        {
            if (name == "Function")
            {
                f = (IFunction)value;
            }
            base.SetProperty(name, value);
        }

        public override void RecalcPoints()
        {
            if (f == null) return;
            Restart();
            ExperimentPoints.Clear();

            Init();
            while (realIteration <= (int)GetProperty("Maximum Iteration"))
            {
                ExperimentPoints.Sort((p1, p2) => p1.evolventX.CompareTo(p2.evolventX));
                var d = new double[realIteration];
                for (int i = 0; i < realIteration; i++)
                {
                    var right = ExperimentPoints[i + 1];
                    var left = ExperimentPoints[i];
                    d[i] = Math.Abs((right.y - left.y) / (right.evolventX - left.evolventX));
                }
                var M = d.Max();
                var m = M > 0 ? M * (double)GetProperty("r") : 1;
                var R = new double[realIteration];
                maxCharIndex = 1;
                double maxR = Double.MinValue;
                for (int i = 0; i < realIteration; i++)
                {
                    var right = ExperimentPoints[i + 1];
                    var left = ExperimentPoints[i];
                    R[i] = m * (right.evolventX - left.evolventX) +
                               ((right.y - left.y) * (right.y - left.y)) / (m * (right.evolventX - left.evolventX)) -
                               2 * (right.y + left.y);
                    if (R[i] > maxR)
                    {
                        maxCharIndex = i + 1;
                        maxR = R[i];
                    }
                }
                if (IsEnded()) return;
                double newPoint = 0.5 *
                                  (ExperimentPoints[maxCharIndex].evolventX +
                                   ExperimentPoints[maxCharIndex - 1].evolventX) -
                                  (ExperimentPoints[maxCharIndex].y - ExperimentPoints[maxCharIndex - 1].y) / (2 * m);
                ExperimentPointsReal.Add(new MethodPoint()
                {
                    evolventX = newPoint,
                    IdFun = 0,
                    iteration = realIteration + 1,
                    y = f.Calc(newPoint),
                    x = f.GetImage(newPoint)
                });

                ExperimentPoints.Add(new MethodPoint()
                {
                    evolventX = newPoint,
                    IdFun = 0,
                    iteration = realIteration + 1,
                    y = f.Calc(newPoint),
                    x = f.GetImage(newPoint)
                });
                realIteration++;
            }
        }

        public override bool IsEnded()
        {
            if (Math.Abs(ExperimentPoints[maxCharIndex].evolventX - ExperimentPoints[maxCharIndex - 1].evolventX)
                < (double)GetProperty("Precision"))
                return true;
            return false;
        }

        private void Init()
        {
            ExperimentPoints.Add(new MethodPoint()
            {
                IdFun = 1,
                iteration = 0,
                isEnd = AlgEnd.no,
                x = f.Left,
                y = f.Calc(f.Left),
                evolventX = 0
            });
            ExperimentPoints.Add(new MethodPoint()
            {
                IdFun = 1,
                iteration = 1,
                isEnd = AlgEnd.no,
                x = f.Right,
                y = f.Calc(f.Right),
                evolventX = 1
            });
            realIteration = 1;
            ExperimentPointsReal.Add(new MethodPoint()
            {
                IdFun = 1,
                iteration = 0,
                isEnd = AlgEnd.no,
                x = f.Left,
                y = f.Calc(f.Left),
                evolventX = 0
            });
            ExperimentPointsReal.Add(new MethodPoint()
            {
                IdFun = 1,
                iteration = 1,
                isEnd = AlgEnd.no,
                x = f.Right,
                y = f.Calc(f.Right),
                evolventX = 1
            });
            realIteration = 1;
        }
    }
}