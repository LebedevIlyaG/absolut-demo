﻿using System;
using System.Linq;

namespace Algorithms
{
    public class ScanningAlg : CharacteristicAlg
    {
        protected override double CalcNewPoint()
        {
            return 0.5*(ExperimentPoints[maxCharIndex].evolventX +
                        ExperimentPoints[maxCharIndex - 1].evolventX);
        }

        protected override double CalcCharacteristic(int curIntervalIndex)
        {
            
            var right = ExperimentPoints[curIntervalIndex + 1];
            var left = ExperimentPoints[curIntervalIndex];
            return right.evolventX - left.evolventX;

        }
    }
}