﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Algorithms.Methods
{
    class ScanningAlg_Paral:CharacteristicAlgParal
    {
            protected override double CalcNewPoint()
            {
                //return 0.5 * (ExperimentPoints[maxCharIndex].evolventX +
                //            ExperimentPoints[maxCharIndex - 1].evolventX);
                return 0.5 * (Characteristic_list[maxCharIndex].right_x.evolventX +
                            Characteristic_list[maxCharIndex].left_x.evolventX);
            }

            protected override double CalcCharacteristic(int curIntervalIndex)
            {

                var right = ExperimentPoints[curIntervalIndex + 1];
                var left = ExperimentPoints[curIntervalIndex];
                Characteristic_list.Add(new Methods.Characteristic()
                {
                    evolventX = right.evolventX,
                    right_x = right,
                    sort_x_interval = curIntervalIndex,
                    left_x = left,
                    value_characteristic = right.evolventX - left.evolventX
                });
                return right.evolventX - left.evolventX;

            }
    }
    
}
