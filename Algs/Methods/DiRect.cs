﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Functions;
using System.Diagnostics;
using System.Globalization;
using System.Xml;
using System.IO;
using Properties;

namespace Algorithms
{
    class DiRect : GenericAlg
    {
        protected IFunction f;
        protected int maxCharIndex;


        public DiRect()
        {
            //RegisterProperty(new DoubleProperty("Precision", 0.000001, 0.1, 5, DoubleDeltaType.Log, 0.00001));
            RegisterProperty(new FuncProperty("Function", null));
            RegisterProperty(new StringProperty("DiRect_dll", ""));
            RegisterProperty(new StringProperty("DiRect_conf", ""));
            RegisterProperty(new StringProperty("DiRect_exe", ""));
            RegisterProperty(new StringProperty("outDiRect", ""));
            
            //RegisterProperty(new IntProperty("Dimension", 2, 2, 0, 2));
            //RegisterProperty(new IntProperty("Evolvent", 8, 12, 1, 10));

        }

        public override void SetProperty(string name, object value)
        {
            if (name == "Function")
            {
                f = (IFunction)value;
            }
            base.SetProperty(name, value);
        }

        protected virtual double CalcCharacteristic(int curIntervalIndex)
        {
            return 0;
        }

        protected virtual double CalcNewPoint()
        {
            return 0;
        }

        public override void RecalcPoints()
        {
            if ((string)GetProperty("DiRect_conf") == "" ||
                (string)GetProperty("DiRect_dll") == "" ||
                (string)GetProperty("DiRect_exe") == "" ||
                (string)GetProperty("outDiRect") == "" ||
                f == null) return;
            Restart();
            ExperimentPoints.Clear();
            Init();

            string path = (string)GetProperty("outDiRect");
            if (File.Exists(path))

                // Открываем файл для чтения и читаем из него все строки, построчно
                using (StreamReader sr = File.OpenText(path))
                {
                    int iter = 0;
                    string firstLine = sr.ReadLine();
                    //string[] info = firstLine.Split(' ');
                    //realIteration = Convert.ToInt32(info[0]);
                    realIteration = Convert.ToInt32(firstLine);
                    while (!sr.EndOfStream)
                    {
                        var s = sr.ReadLine();
                        
                        string[] point = s.Split(' ');
                        MethodPoint mp;
                        mp = new MethodPoint
                        {
                            iteration = iter
                        };
                        List<double> x_iter = new List<double>();
                        mp.x.Add(Convert.ToDouble(point[0], CultureInfo.InvariantCulture));
                        mp.x.Add(Convert.ToDouble(point[1], CultureInfo.InvariantCulture));
                        mp.y = f.Calc(mp.x);
                        iter++;
                        mp.id_process = 0;
                        ExperimentPointsReal.Add(mp);

                    }
                }
        }
        
        public override bool IsEnded()
        {
            return true;
        }

        private void Init()
        {
           
            if ((string)GetProperty("DiRect_conf") == "" || (string)GetProperty("DiRect_dll") == "" ||
                (string)GetProperty("DiRect_exe") == "")
                return;
            Process DiRect_Process = new Process()
            {
                StartInfo = new ProcessStartInfo()
                {
                    FileName = (string)GetProperty("DiRect_exe"),
                    Arguments = (string)GetProperty("DiRect_dll") + (string)GetProperty("DiRect_conf") +
                    (string)GetProperty("outDiRect"),
                    
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true
                }
            };

            DiRect_Process.Start();

            while (!DiRect_Process.HasExited) ;
            var output = "";
            while (!DiRect_Process.StandardOutput.EndOfStream)
            {
                output += DiRect_Process.StandardOutput.ReadLine();
                // do something with line
            }
        }
    }
}
