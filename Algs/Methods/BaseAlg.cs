﻿using System.Collections.Generic;
using Functions;
using Properties;

namespace Algorithms
{
    public class BaseAlg : ISearchAlg
    {
        protected List<MethodPoint> AllPoints;
        protected List<MethodPoint> AllPoints_number;
        protected int multidimensionality; //нужен для обнаружения многомерных и одномерных задач
        protected Dictionary<string, PropertyList_class> PropertiesList;
        protected Dictionary<string, double> PropertiesList_d;
        protected Dictionary<string, IFunction> PropertiesList_f;
        protected Dictionary<string, int> PropertiesList_i;

        public BaseAlg()
        {
            Iterations = 0;
        }
        public int IterOverMin()
        {
            return 0;
        }

        public bool IsIterOverMin()
        {
            return false;
        }

        public int MaxIterations { get; set; }
        public int real_iteration { get; set; }

        public int Iterations { get; set; }
        public string Name { get; set; }

        public void Restart()
        {
            Iterations = -1;
        }

        void ISearchAlg.NextStep()
        {
            throw new System.NotImplementedException();
        }

        public void PrevStep()
        {
            throw new System.NotImplementedException();
        }

        public bool HasNext()
        {
            throw new System.NotImplementedException();
        }

        public int CurrentIter()
        {
            throw new System.NotImplementedException();
        }

        public MethodPoint LastPoint()
        {
            throw new System.NotImplementedException();
        }

        public MethodPoint Argmin()
        {
            var minX = AllPoints[0];
            var y = AllPoints[0].y;
            for (var i = 1; i <= Iterations; i++)
            {
                if (AllPoints[i].y < y)
                {
                    y = AllPoints[i].y;
                    minX = AllPoints[i];
                }
            }
            return minX;
        }

        public MethodPoint ArgminTask()
        {
            var iter = 0;
            var y = AllPoints[0].y;
            for (var i = 1; (i < MaxIterations) && (i < Iterations); i++)
            {
                if (AllPoints[i].y < y)
                {
                    y = AllPoints[i].y;
                    iter++;
                }
            }
            return AllPoints[iter];
        }

        public virtual bool IsEnded()
        {
            return false;
        }

        public virtual void RecalcPoints()
        {
        }

        public IEnumerable<MethodPoint> CalculatedPoints(int start)
        {
            int k;
            MethodPoint point;
            if (real_iteration <= MaxIterations) k = real_iteration;
            else k = MaxIterations;
            for (var i = start; i < k; i++)
            {
                // point=NextStep();
                yield return AllPoints[i];
            }
        }

        public virtual ISearchAlg Clone()
        {
            var t = (BaseAlg) MemberwiseClone();
            return t;
        }

        public void SetProperty(string name, object value)
        {
            throw new System.NotImplementedException();
        }

        public void SetProperties(Dictionary<string, object> propsValues)
        {
            throw new System.NotImplementedException();
        }

        public object GetProperty(string name)
        {
            throw new System.NotImplementedException();
        }

        public List<PropertyInfo> GetPropertiesInfo()
        {
            throw new System.NotImplementedException();
        }

        public MethodPoint LastCalculatedPoint()
        {
            return AllPoints[Iterations];
        }


        public virtual void SetDoubleProperty(string name, double value)
        {
            if (!PropertiesList.ContainsKey(name))
            {
                PropertiesList.Add(name, new PropertyList_class(PropertyType.Double, 0, 10000, 0.1));
            }
        }


        public virtual void SetIntegerProperty(string name, int value)
        {
            if (!PropertiesList.ContainsKey(name))
            {
                PropertiesList.Add(name, new PropertyList_class(PropertyType.Integer, 0, 10000, 0.1));
            }
        }


        public virtual void SetFuncProperty(string name, IFunction value)
        {
            if (!PropertiesList.ContainsKey(name))
            {
                PropertiesList.Add(name, new PropertyList_class(PropertyType.Func, value.Left[0], value.Right[0], 0.0));
            }
        }


        public virtual double GetDoubleProperty(string name)
        {
            double t = 1;
            if (PropertiesList.ContainsKey(name))
            {
                if (name == "Double") return t;
            }
            return 0;
        }


        public virtual int GetIntegerProperty(string name)
        {
            var t = 1;
            if (PropertiesList.ContainsKey(name))
            {
                if (name == "Integer") return t;
            }
            return 0;
        }


        public virtual IFunction GetFuncProperty(string name)
        {
            IFunction Func = null;
            if (PropertiesList.ContainsKey(name))
            {
                if (name == "Function") return Func;
            }
            return null;
        }


        public virtual Dictionary<string, PropertyList_class> GetPropertiesNames()
        {
            return PropertiesList;
        }


        public virtual void InitPropertyList() //protected?
        {
        }

        public MethodPoint NextStep()
        {
            Iterations++;
            if (AllPoints.Count < MaxIterations)
            {
                if (AllPoints.Count == 0) return null;
                return AllPoints[Iterations - 1];
            }
            return null;
        }
    }
}