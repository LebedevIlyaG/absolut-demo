﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;

using System.Threading;
using System.Xml;
using Functions;
using Properties;


namespace Algorithms
{

    //public abstract class PropertyInfo
    //{
    //    public string name { get; protected set; }
    //    public object defaultValue { get; protected set; }
    //    public abstract bool CheckValue(object value);
    //    public PropertyType Type { get; protected set; }
    //}

    //public enum DoubleDeltaType
    //{
    //    Step,
    //    Log
    //}
    //public class DoubleProperty : PropertyInfo
    //{
    //    public double min { get; protected set; }
    //    public double max { get; protected set; }
    //    public int delta { get; set; }
    //    public DoubleDeltaType deltaType { get; protected set; }



    //    public DoubleProperty(string name, double min, double max, int delta, DoubleDeltaType type, double defaultValue)
    //    {
    //        Type = PropertyType.Double;
    //        if (defaultValue > max || defaultValue < min) throw new ArgumentException();
    //        if(delta <= 0 ) throw new ArgumentOutOfRangeException();
    //        this.name = name;
    //        this.min = min;
    //        this.max = max;
    //        this.delta = delta;
    //        this.defaultValue = defaultValue;
    //        deltaType = type;
    //    }


    //    public override bool CheckValue(object value)
    //    {
    //        double val = 0;
    //        try
    //        {
    //            val = (double)value;
    //        }
    //        catch (Exception)
    //        {

    //            return false;
    //        }
    //        return !(val < min) && !(val > max);
    //    }
    //}

    //public class IntProperty : PropertyInfo
    //{
    //    public int min { get; protected set; }
    //    public int max { get; protected set; }
    //    public int delta { get; set; }
    //    public IntProperty(string name, int min, int max, int delta, int defaultValue)
    //    {
    //        Type = PropertyType.Integer;
    //        if(defaultValue > max || defaultValue < min) throw new ArgumentException();
    //        this.name = name;
    //        this.min = min;
    //        this.max = max;
    //        this.delta = delta;
    //        this.defaultValue = defaultValue;
    //    }

    //    public override bool CheckValue(object value)
    //    {
    //        int val;
    //        try
    //        {
    //            val = (int) value;
    //        }
    //        catch (Exception)
    //        {

    //            return false;
    //        }
    //        return !(val < min) && !(val > max);
    //    }
    //}
    //public class FuncProperty : PropertyInfo
    //{
    //    public FuncProperty(string name, IFunction defaultValue)
    //    {
    //        Type = PropertyType.Func;
    //        this.name = name;
    //        this.defaultValue = defaultValue;
    //    }

    //    public override bool CheckValue(object value)
    //    {
    //        IFunction f = value as IFunction;
    //        if (f == null)
    //        {
    //            return false;
    //        }
    //        return true;
    //    }
    //}
    //public class FuncListProperty : PropertyInfo
    //{
    //    public FuncListProperty(string name, List<IFunction> defaultValue )
    //    {
    //        this.name = name;
    //        this.defaultValue = defaultValue;
    //    }

    //    public override bool CheckValue(object value)
    //    {
    //        List<IFunction> f = value as List<IFunction>;
    //        if (f == null)
    //        {
    //            return false;
    //        }
    //        return true;
    //    }
    //}



    public class GenericAlg : ISearchAlg, IPropertyUser
    {

        protected int iteration = 0;
        protected int realIteration = 0;
        protected int IterationOverMin = 0;
        protected bool IterMin = false;
        public string Name => "Generic algorithm";
        private int maxIterations => (int) p.GetProperty("Maximum Iteration");
        protected List<MethodPoint> ExperimentPoints;
        protected List<MethodPoint> ExperimentPointsReal;
        //protected Dictionary<string, object> AlgProperties;
        //protected readonly List<PropertyInfo> PropertyInfos;
        protected PropertyProvider p = new PropertyProvider();

        public GenericAlg()
        {
            ExperimentPointsReal = new List<MethodPoint>();
            ExperimentPoints = new List<MethodPoint>();
           // AlgProperties = new Dictionary<string, object>();
          //  PropertyInfos = new List<PropertyInfo>();
          
            RegisterProperty(new IntProperty("Maximum Iteration", 0, 10000, 10, 100));
        }

        public void Restart()
        {
            IterMin = false;
            IterationOverMin = 0;
            iteration = 0;
        }

        public void NextStep()
        {
            if (iteration+1<=realIteration)
           
                iteration++;

        }

        public void PrevStep()
        {
            if (iteration > 0 && iteration <= realIteration)
                iteration--;
        }

        public bool HasNext()
        {
            if (iteration + 1 <= realIteration)
                return true;
            else return false;
        }

        public int CurrentIter()
        {
            return iteration;
        }

        public int IterOverMin()
        {
            if(IterMin==true) return IterationOverMin;
            return realIteration;
        }

        public bool IsIterOverMin()
        {
            return IterMin;
        }

        public MethodPoint Argmin()
        {
            if (ExperimentPoints.Count == 0) throw new AccessViolationException("Experiment points is empty");
            double min = ExperimentPoints.Min(point => point.y);
            return ExperimentPoints.Find(point => Math.Abs(point.y - min) < Double.Epsilon);

        }

        public MethodPoint ArgminTask()
        {
            if (iteration == 0) return null;
            if (ExperimentPointsReal.Count == 0) return null;
            var iter = 1;
            var y = Double.MaxValue;
            for (var i = 0; (i < maxIterations) && (i < iteration); i++)
            {
                if (ExperimentPointsReal[i].y < y)
                {
                    y = ExperimentPointsReal[i].y;
                    iter = i;
                }
            }
            return ExperimentPointsReal[iter];
        }

        public virtual bool IsEnded()
        {
            return true;
        }

        public virtual void RecalcPoints()
        {

        }

        public IEnumerable<MethodPoint> CalculatedPoints(int start = 0)
        {
            return ExperimentPointsReal.GetRange(0, iteration).AsReadOnly();
        }

        public ISearchAlg Clone()
        {
            return (ISearchAlg)this.MemberwiseClone();
        }

        public MethodPoint LastPoint()
        {
            return ExperimentPointsReal[iteration];
            //return ExperimentPoints[iteration];
        }

        private void SetPropertyWithoutRecalc(string name, object value)
        {
           p.SetProperty(name, value);
        }
        public virtual void SetProperty(string name, object value)
        {
            SetPropertyWithoutRecalc(name, value);
            // Thread calculator = new Thread(RecalcPoints);
            //calculator.Start();
            RecalcPoints();

        }

        public void SetProperties(Dictionary<string, object> propsValues)
        {
            foreach (var k in propsValues)
            {
                SetPropertyWithoutRecalc(k.Key, k.Value);
            }
            RecalcPoints();
        }

        public object GetProperty(string name)
        {
            return p.GetProperty(name);
        }

      

        public List<PropertyInfo> GetPropertiesInfo()
        {
            return p.GetPropertiesInfo();
        }

    

       public void RegisterProperty(PropertyInfo info)
        {
            p.RegisterProperty(info);
        }
    }
}