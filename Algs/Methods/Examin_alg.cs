﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Functions;
using System.Diagnostics;
using System.Globalization;
using System.Xml;
using System.IO;
using Properties;

namespace Algorithms
{
    class Examin_alg : GenericAlg
    {
        protected IFunction f;
        protected int maxCharIndex;


        public Examin_alg()
        {
            RegisterProperty(new DoubleProperty("Precision", 0.000001, 0.1, 5, DoubleDeltaType.Log, 0.00001));
            RegisterProperty(new FuncProperty("Function", null));
            RegisterProperty(new StringProperty("Examin_dll", ""));
            RegisterProperty(new StringProperty("Examin_conf", ""));
            RegisterProperty(new StringProperty("Examin_exe", ""));
            RegisterProperty(new IntProperty("Dimension", 2, 2, 0, 2));
            RegisterProperty(new IntProperty("Evolvent", 8, 12, 1, 10));
            RegisterProperty(new DoubleProperty("r", 1.000001, 100, 100, DoubleDeltaType.Step, 2));
        }

        public override void SetProperty(string name, object value)
        {
            if (name == "Function")
            {
                f = (IFunction) value;
            }
            base.SetProperty(name, value);
        }

        protected virtual double CalcCharacteristic(int curIntervalIndex)
        {
            return 0;
        }

        protected virtual double CalcNewPoint()
        {
            return 0;
        }

        public override void RecalcPoints()
        {
            if ((string) GetProperty("Examin_conf") == "" ||
                (string) GetProperty("Examin_dll") == "" ||
                (string) GetProperty("Examin_exe") == "" ||
                f == null) return;
            Restart();
            ExperimentPointsReal.Clear();
            Init();

            string path = @"out_point.txt";
            if (File.Exists(path))

                // Открываем файл для чтения и читаем из него все строки, построчно
                using (StreamReader sr = File.OpenText(path))
                {
                    int iter = 0;
                    string firstLine = sr.ReadLine();
                    string[] info = firstLine.Split(' ');
                    realIteration = Convert.ToInt32(info[0]);

                    while (!sr.EndOfStream)
                    {
                        var s = sr.ReadLine();
                        string[] parts = s.Split('|');
                        if(parts.Length == 1) continue;
                        string[] point = parts[0].Split(' ');
                        MethodPoint mp;
                        mp = new MethodPoint
                        {
                            iteration = iter
                        };
                        mp.y = Convert.ToDouble(parts[1].TrimStart(' '), CultureInfo.InvariantCulture);
                        mp.x.Add(Convert.ToDouble(point[0], CultureInfo.InvariantCulture));
                        mp.x.Add(Convert.ToDouble(point[1], CultureInfo.InvariantCulture));
                        iter++;
                        mp.id_process = 0;
                        ExperimentPointsReal.Add(mp);

                    }
                   

                    //while (!sr.EndOfStream)
                    //{ 
                    //     s = sr.ReadLine();
                    //    string point_str = "";
                    //    if (first_line == 1)
                    //    {
                    //        s += " ";
                    //        int i = 0;
                    //        while (s[i] != ' ')
                    //        {
                    //            point_str += s[i];
                    //            i++;
                    //        }
                    //        realIteration = Convert.ToInt32(point_str);
                    //        first_line++;
                    //    }
                    //    else
                    //    {
                    //        int r = 0;
                    //        for (int j = 0; j < point_str.Length; j++)
                    //            if (point_str[j] == '|') r = 1;
                    //        if (r == 1)
                    //        {
                    //            MethodPoint point = new MethodPoint();
                    //            int i = 0, t1 = 0;
                    //            string funct = "";
                    //            while (s[i] != '|')
                    //            {
                    //                point_str += s[i];
                    //            }
                    //            i += 2;
                    //            while (s[i] != ' ')
                    //            {
                    //                funct += s[i];
                    //            }

                    //            for (int j = 0; j < point_str.Length; j++)
                    //                if (point_str[j] == ' ') t1++;

                    //            string[] point_str_mas = new string[t1];
                    //            for (int j = 0; j < t1; j++) point_str_mas[j] = "";
                    //            int t2 = 0;
                    //            for (int j = 0; j < point_str.Length; j++)
                    //            {
                    //                if (point_str[j] == ' ') t2++;
                    //                else point_str_mas[t2] += point_str[j];
                    //            }
                    //            for (int j = 0; j < t1; j++) point.x.Add(Convert.ToDouble(point_str_mas[j]));
                    //            point.y = Convert.ToDouble(funct);


                }
        }
   

    

        public override bool IsEnded()
        {
            return true;
        }

        private void Init()
        {
            if ((string) GetProperty("Examin_conf") == "" || (string) GetProperty("Examin_dll") == "" ||
                (string) GetProperty("Examin_exe") == "")
                return;
            var t = "-Dimension " + ((int) GetProperty("Dimension")).ToString() +
                    " -r " + ((double) GetProperty("r")).ToString(CultureInfo.InvariantCulture) +
                    " -Eps " + ((double) GetProperty("Precision")).ToString(CultureInfo.InvariantCulture) +
                    " -m " + (int) GetProperty("Evolvent") +
                    " -MaxNumOfPoints " + (int) GetProperty("Maximum Iteration") +
                    " -sip " + @"""out_point.txt""" +
                    " - libConf " + @"""" + ((string)GetProperty("Examin_conf")).Replace("//", "/") + @"""" +
                    " -lib "+ @"""" +((string) GetProperty("Examin_dll")).Replace("//", "/") + @"""";
            Process Examin_Process = new Process() {StartInfo =  new ProcessStartInfo()
            {
                FileName = (string)GetProperty("Examin_exe"),
                Arguments = t,
                UseShellExecute = false,
                RedirectStandardOutput = true,
                CreateNoWindow = true
            } };

            Examin_Process.Start();
          
            while (!Examin_Process.HasExited) ;
            var output = "";
            var fileStream = File.Create(@"examinOutput.txt");

            Examin_Process.StandardOutput.BaseStream.CopyTo(fileStream);
            
        }
    }
}