﻿using System;
using System.Linq;
using Properties;

namespace Algorithms
{
    public class Agp:CharacteristicAlg
    {
        private double m;
        public Agp()
        {
            RegisterProperty(new DoubleProperty("r", 1.000001, 100,100,DoubleDeltaType.Step, 2));
        }

        protected override double CalcNewPoint()
        {
            return 0.5 * (ExperimentPoints[maxCharIndex].evolventX +
                                   ExperimentPoints[maxCharIndex - 1].evolventX) -
                                  (ExperimentPoints[maxCharIndex].y - ExperimentPoints[maxCharIndex - 1].y) / (2 * m);
        }

        protected override double CalcCharacteristic(int curIntervalIndex)
        {
            if (curIntervalIndex == 0)
            {
                var d = new double[realIteration];
                for (int i = 0; i < realIteration; i++)
                {
                    var right = ExperimentPoints[i + 1];
                    var left = ExperimentPoints[i];
                    d[i] = Math.Abs((right.y - left.y) / (right.evolventX - left.evolventX));
                }
                var M = d.Max();
                 m = M > 0 ? M * (double)GetProperty("r") : 1;
            }

            var point = ExperimentPoints[curIntervalIndex + 1];
            var methodPoint = ExperimentPoints[curIntervalIndex];
            return m * (point.evolventX - methodPoint.evolventX) +
                               ((point.y - methodPoint.y) * (point.y - methodPoint.y)) / (m * (point.evolventX - methodPoint.evolventX)) -
                               2 * (point.y + methodPoint.y);
        }
    }
}