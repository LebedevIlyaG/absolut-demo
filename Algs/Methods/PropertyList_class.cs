﻿namespace Algorithms
{
    public enum PropertyType
    {
        Double,
        Integer,
        Func,
        String
    }

    public class PropertyList_class
    {
        public double Increase;
        public double Left;
        public double Right;
        public PropertyType TypeParametr;

        public PropertyList_class(PropertyType _TypeParametr = PropertyType.Double, double _Left = 0,
            double _Right = 10000, double _Increase = 0.00001)
        {
            TypeParametr = _TypeParametr;
            Left = _Left;
            if (_Right < Left) Right = Left + 1;
            else Right = _Right;
            if (TypeParametr != PropertyType.Func)
            {
                if (_Increase < 0) Increase = 0.1;
                else Increase = _Increase;
            }
            else Increase = 0;
        }

        ~PropertyList_class()
        {
        }
    }
}