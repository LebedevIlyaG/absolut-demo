﻿using System;
using System.Linq;
using Functions;

using System.Collections.Generic;
using Properties;

namespace Algorithms
{
    public class CharacteristicAlg : GenericAlg
    {
        protected IFunction f;
        protected int maxCharIndex = 1;
        protected List<Methods.Characteristic> Characteristic_list;

        public CharacteristicAlg()
        {
            Characteristic_list = new List<Methods.Characteristic>();
            RegisterProperty(new DoubleProperty("Precision", 0.000001, 0.1, 6, DoubleDeltaType.Log, 0.001));
            RegisterProperty(new FuncProperty("Function", null));
            
        }

        public override void SetProperty(string name, object value)
        {
            if (name == "Function")
            {
                f = (IFunction)value;
            }
            base.SetProperty(name, value);
        }

        protected virtual double CalcCharacteristic(int curIntervalIndex)
        {
            return 0;
        }

        protected virtual double CalcNewPoint()
        {
            return 0;
        }
        public override void RecalcPoints()
        {
            if (f == null) return;
            maxCharIndex = 1;
            Restart();
            ExperimentPoints.Clear();
            ExperimentPointsReal.Clear();
            Init();
            while (realIteration <= (int)GetProperty("Maximum Iteration") && !IsEnded())
            {
                ExperimentPoints.Sort((p1, p2) => p1.evolventX.CompareTo(p2.evolventX));
                maxCharIndex = 1;
                double maxR = Double.MinValue;
                Characteristic_list.Clear();
                for (int i = 0; i < realIteration; i++)
                {
                    var R = CalcCharacteristic(i);
                    if (R > maxR)
                    {
                        maxCharIndex = i + 1;
                        maxR = R;
                    }
                }
                if (f.Min_x!=null && IsEndedOverMin()) IterMin = true;
                if (IsEnded()) return;
                double newPoint = CalcNewPoint();
                if (!IsIterOverMin())
                {
                    IterationOverMin++;
                }

                ExperimentPointsReal.Add(new MethodPoint()
                {
                    evolventX = newPoint,
                    IdFun = 0,
                    iteration = realIteration + 1,
                    y = f.Calc(newPoint),
                    x = f.GetImage(newPoint)
                });

                ExperimentPoints.Add(new MethodPoint()
                {
                    evolventX = newPoint,
                    IdFun = 0,
                    iteration = realIteration + 1,
                    y = f.Calc(newPoint),
                    x = f.GetImage(newPoint)
                });
                realIteration++;
            }
        }

        public void Step()
        {

        }

        public void AddPoint()
        {

        }

        public void InitReduction()
        {

        }

        public override bool IsEnded()
        {
            if (Math.Abs(ExperimentPoints[maxCharIndex].evolventX - ExperimentPoints[maxCharIndex - 1].evolventX)
                < (double)GetProperty("Precision"))
                return true;
            return false;
        }

        public bool IsEndedOverMin()
        {
            if ((!IterMin) && (Math.Abs(ExperimentPoints[maxCharIndex].evolventX - f.GetPrototype(f.Min_x)) < (double)GetProperty("Precision")))
            {
                IterMin = true;
                return true;
            }
            return false;
        }

        private void Init()
        {
            ExperimentPoints.Add(new MethodPoint()
            {
                IdFun = 1,
                iteration = 0,
                isEnd = AlgEnd.no,
                x = f.Left,
                y = f.Calc(f.Left),
                evolventX = 0
            });
            ExperimentPoints.Add(new MethodPoint()
            {
                IdFun = 1,
                iteration = 1,
                isEnd = AlgEnd.no,
                x = f.Right,
                y = f.Calc(f.Right),
                evolventX = 1
            });
            ExperimentPointsReal.Add(new MethodPoint()
            {
                IdFun = 1,
                iteration = 0,
                isEnd = AlgEnd.no,
                x = f.Left,
                y = f.Calc(f.Left),
                evolventX = 0
            });
            ExperimentPointsReal.Add(new MethodPoint()
            {
                IdFun = 1,
                iteration = 1,
                isEnd = AlgEnd.no,
                x = f.Right,
                y = f.Calc(f.Right),
                evolventX = 1
            });
            realIteration = 1;
        }
    }
}