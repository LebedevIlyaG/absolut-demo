﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Functions;
using Properties;

namespace Algorithms.Methods
{
    class CharacteristicAlgParal : GenericAlg
    {
        protected IFunction f;
        protected int maxCharIndex;
        protected List<Methods.Characteristic> Characteristic_list;

        public CharacteristicAlgParal()
        {

            Characteristic_list = new List<Methods.Characteristic>();
            RegisterProperty(new DoubleProperty("Precision", 0.000001, 0.1, 5, DoubleDeltaType.Log, 0.00001));
            RegisterProperty(new FuncProperty("Function", null));
            RegisterProperty(new IntProperty("Process", 1, 4, 3, 2));

        }

        public override void SetProperty(string name, object value)
        {
            if (name == "Function")
            {
                f = (IFunction)value;
            }
            base.SetProperty(name, value);

        }

        protected virtual double CalcCharacteristic(int curIntervalIndex)
        {
            return 0;
        }

        protected virtual double CalcNewPoint()
        {
            return 0;
        }
        public override void RecalcPoints()
        {
            if (f == null) return;
            Restart();
            ExperimentPoints.Clear();
            Init();

            while (realIteration < (int)GetProperty("Maximum Iteration"))
            {
                ExperimentPoints.Sort((p1, p2) => p1.evolventX.CompareTo(p2.evolventX));
                maxCharIndex = 0;
                
                Characteristic_list.Clear();
                for (int i = 0; i < realIteration; i++)
                {
                    var R = CalcCharacteristic(i);

                }
                Characteristic_list.Sort((p1, p2) => p2.value_characteristic.CompareTo(p1.value_characteristic));

                int index = 0;
                if ((int)GetProperty("Process") > Characteristic_list.Count) index = Characteristic_list.Count;
                else index = (int)GetProperty("Process");


                for (int i = 0; i < index; i++)
                {


                    double newPoint = CalcNewPoint();

                    
                    

                    ExperimentPointsReal.Add(new MethodPoint()
                    {
                        evolventX = newPoint,
                        IdFun = 0,
                        iteration = realIteration + 1,
                        y = f.Calc(newPoint),
                        x = f.GetImage(newPoint),
                        id_process = i
                    });

                    ExperimentPoints.Add(new MethodPoint()
                    {
                        evolventX = newPoint,
                        IdFun = 0,
                        iteration = realIteration + 1,
                        y = f.Calc(newPoint),
                        x = f.GetImage(newPoint),
                        id_process = i
                    });
                    realIteration++;
                    if (f.Min_x!=null && IsEndedOverMin()) IterMin = true;
                    if (!IsIterOverMin())
                    {
                        IterationOverMin++;
                    }
                    if (IsEnded()) return;
                    maxCharIndex = i + 1;
                }
                

            }
        }

        public override bool IsEnded()
        {
            // if (Math.Abs(ExperimentPoints[maxCharIndex].evolventX - ExperimentPoints[maxCharIndex - 1].evolventX)
            //     < (double)AlgProperties["Precision"])
            if (Math.Pow(Math.Abs(Characteristic_list[maxCharIndex].right_x.evolventX - Characteristic_list[maxCharIndex].left_x.evolventX),0.5)
                    < (double)GetProperty("Precision"))
                return true;
            return false;
        }
        public bool IsEndedOverMin()
        {
            if ((!IterMin) && (Math.Abs(ExperimentPoints[maxCharIndex].evolventX - f.GetPrototype(f.Min_x)) < (double)GetProperty("Precision")))
            {
                IterMin = true;
                return true;
            }
            return false;
        }

        private void Init()
        {
            ExperimentPoints.Add(new MethodPoint()
            {
                IdFun = 1,
                iteration = 0,
                isEnd = AlgEnd.no,
                x = f.Left,
                y = f.Calc(f.Left),
                evolventX = 0,
                id_process = 0
            });
            ExperimentPoints.Add(new MethodPoint()
            {
                IdFun = 1,
                iteration = 1,
                isEnd = AlgEnd.no,
                x = f.Right,
                y = f.Calc(f.Right),
                evolventX = 1,
                id_process = 0
            });
            ExperimentPointsReal.Add(new MethodPoint()
            {
                IdFun = 1,
                iteration = 0,
                isEnd = AlgEnd.no,
                x = f.Left,
                y = f.Calc(f.Left),
                evolventX = 0,
                id_process = 0
            });
            ExperimentPointsReal.Add(new MethodPoint()
            {
                IdFun = 1,
                iteration = 1,
                isEnd = AlgEnd.no,
                x = f.Right,
                y = f.Calc(f.Right),
                evolventX = 1,
                id_process = 0
            });
            realIteration = 1;
        }
    }
}
