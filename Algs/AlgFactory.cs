﻿using System;
using System.Collections.Generic;


namespace Algorithms
{
    public static class AlgFactory
    {
        public static List<string> Options = new List<string>() {"Agp", "Scanning alg", "AGP_paral", "Scanning_alg_paral" };
        public static ISearchAlg Build(string algName)
        {
            if(algName == "Agp")
                return new Agp();
            if(algName == "Scanning alg")
                return new ScanningAlg();
            if (algName == "AGP_paral")
                return new Algorithms.Methods.AGP_2();
            if (algName == "Scanning_alg_paral")
                return new Algorithms.Methods.ScanningAlg_Paral();
            
            throw new ArgumentException(algName);
        }

        public static ISearchAlg BuildExaminAlg(string dll, string conf, string examin)
        {
            ISearchAlg a = new Examin_alg();
            a.SetProperty("Examin_conf", conf);
            a.SetProperty("Examin_dll", dll);
            a.SetProperty("Examin_exe", examin);
            return a;
        }

        public static ISearchAlg BuildDiRectAlg(string dll, string conf, string examin)// string outfile)
        {
            ISearchAlg a = new DiRect();
            a.SetProperty("DiRect_conf", conf);
            a.SetProperty("DiRect_dll", dll);
            a.SetProperty("DiRect_exe", examin);
            a.SetProperty("outDiRect", "outDirect.txt");
            return a;
        }
    }
}