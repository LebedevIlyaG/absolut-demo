﻿using System.Collections.Generic;
using Functions;
using Properties;

namespace Algorithms
{
    public interface ISearchAlg
    {
        string Name { get; }
        void Restart();
        void NextStep();
        void PrevStep();
        bool HasNext();
        int CurrentIter();
        MethodPoint LastPoint();
        MethodPoint Argmin();
        MethodPoint ArgminTask();
        bool IsEnded();
        void RecalcPoints();
        IEnumerable<MethodPoint> CalculatedPoints(int start = 0);
        ISearchAlg Clone();
        void SetProperty(string name, object value);
        void SetProperties(Dictionary<string, object> propsValues);
        object GetProperty(string name);
        List<PropertyInfo> GetPropertiesInfo();
        int IterOverMin();
        bool IsIterOverMin();
    }
}